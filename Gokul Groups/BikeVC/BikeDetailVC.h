//
//  BikeDetailVC.h
//  Gokul Groups
//
//  Created by Shivang on 15/07/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImage.h"

@interface BikeDetailVC : UIViewController{
    
    IBOutlet UITableView *tblviewC;
    NSMutableArray * colorary;
    NSMutableArray * colorary1;
    IBOutlet UIView *hview;

    
    IBOutlet UITableView *tblviewE;
    NSMutableArray * engineary;
    NSMutableArray * engineary1;
    IBOutlet UIView *hview1;

    
    IBOutlet UITableView *tblviewD;
    NSMutableArray * diary;
    NSMutableArray * diary1;
    IBOutlet UIView *hview2;
    
    IBOutlet UITableView *tblviewW;
    NSMutableArray * wheelary;
    NSMutableArray * wheelary1;
    IBOutlet UIView *hview3;
  
    IBOutlet UITableView *tblviewT;
    NSMutableArray * transary;
    NSMutableArray * transary1;
    IBOutlet UIView *hview4;

    IBOutlet UITableView *tblviewCh;
    NSMutableArray * chesary;
    NSMutableArray * chesary1;
    IBOutlet UIView *hview5;
 
    IBOutlet UITableView *tblviewS;
    NSMutableArray * susary;
    NSMutableArray * susary1;
    IBOutlet UIView *hview6;

    IBOutlet UITableView *tblviewEle;
    NSMutableArray * eleary;
    NSMutableArray * eleary1;
    IBOutlet UIView *hview7;

    IBOutlet UITableView *tblviewWeight;
    NSMutableArray * weightary;
    NSMutableArray * weightary1;
    IBOutlet UIView *hview8;

    
    IBOutlet UIScrollView *scrlvw;
    
    int space;
    
    IBOutlet AsyncImage *pic;
    IBOutlet UILabel *lblname;
    
    
}
-(IBAction)backbtnclick:(id)sender;

@property(retain,nonatomic)NSMutableArray * arrdetail;
@end
