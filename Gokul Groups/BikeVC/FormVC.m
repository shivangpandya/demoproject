//
//  FormVC.m
//  Gokul Groups
//
//  Created by Shivang on 08/06/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import "FormVC.h"
#import "UserDefaultHelper.h"

@interface FormVC (){
    NSString * groupis;
    NSString * subgroupis;

}

@end

@implementation FormVC
@synthesize isfrom,detailis;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNeedsStatusBarAppearanceUpdate];

    sendbtn.layer.borderWidth=2.0f;
    sendbtn.layer.borderColor=[UIColor redColor].CGColor;
    sendbtn.layer.cornerRadius=3.0f;
    cancelbtn.layer.borderWidth=2.0f;
    cancelbtn.layer.borderColor=[UIColor redColor].CGColor;
    cancelbtn.layer.cornerRadius=3.0f;
    
    
    
    if ([[UserDefaultHelper sharedObject] userInfo]) {
        tfname.text = [[[UserDefaultHelper sharedObject] userInfo] valueForKey:@"name"];
        tfphone.text = [[[UserDefaultHelper sharedObject] userInfo] valueForKey:@"phone"];
        tfemail.text = [[[UserDefaultHelper sharedObject] userInfo] valueForKey:@"email"];
    }
    
    if ([isfrom isEqualToString:@"inquiry"]) {
        lblheader.text = @"HERO";
        lbltitle.text=@"Inquiry For New Bike";
        groupis = @"HERO";
        subgroupis=@"INQUIRY_FOR_NEW_BIKE";
        tfmsg.text = detailis;
    }
    if ([isfrom isEqualToString:@"service"]) {
        lblheader.text = @"HERO";
        lbltitle.text=@"Service My Bike";
        groupis = @"HERO";
        subgroupis=@"SERVICE_MY_BIKE";
    }
    if ([isfrom isEqualToString:@"parts"]) {
        lblheader.text = @"HERO";
        lbltitle.text=@"Inquiry For Spare Parts";
        groupis = @"HERO";
        subgroupis=@"INQUIRY_FOR_SPARE_PARTS";
    }
    if ([isfrom isEqualToString:@"exchange"]) {
        lblheader.text = @"HERO";
        lbltitle.text=@"Exchange Your Old Bike";
        groupis = @"HERO";
        subgroupis=@"EXCHANGE_YOUR_OLD_BIKE";
    }
    if ([isfrom isEqualToString:@"insurance"]) {
        lblheader.text = @"HERO";
        lbltitle.text=@"Insurance Renewal";
        groupis = @"HERO";
        subgroupis=@"INSURANCE_RENEWAL";
    }
    if ([isfrom isEqualToString:@"career"]) {
        lblheader.text = @"HERO";
        lbltitle.text=@"Career";
        groupis = @"HERO";
        subgroupis=@"CAREER";
    }
    if ([isfrom isEqualToString:@"feedback"]) {
        lblheader.text = @"HERO";
        lbltitle.text=@"Feedback/Query ";
        groupis = @"HERO";
        subgroupis=@"FEEDBACK_QUERY";
    }
    
    if ([isfrom isEqualToString:@"cinquiry"]) {
        lblheader.text = @"FORD";
        lbltitle.text=@"Inquiry For New Car";
        groupis = @"FORD";
        subgroupis=@"INQUIRY_FOR_NEW_CAR";
        tfmsg.text = detailis;

    }
    if ([isfrom isEqualToString:@"cservice"]) {
        lblheader.text = @"FORD";
        lbltitle.text=@"Service My Car";
        groupis = @"FORD";
        subgroupis=@"SERVICE_MY_CAR";
    }
    if ([isfrom isEqualToString:@"cparts"]) {
        lblheader.text = @"FORD";
        lbltitle.text=@"Inquiry For Spare Parts";
        groupis = @"FORD";
        subgroupis=@"INQUIRY_FOR_SPARE_PARTS";
    }
    if ([isfrom isEqualToString:@"cexchange"]) {
        lblheader.text = @"FORD";
        lbltitle.text=@"Exchange Your Old Car";
        groupis = @"FORD";
        subgroupis=@"EXCHANGE_YOUR_OLD_CAR";
    }
    if ([isfrom isEqualToString:@"cinsurance"]) {
        lblheader.text = @"FORD";
        lbltitle.text=@"Insurance Renewal";
        groupis = @"FORD";
        subgroupis=@"INSURANCE_RENEWAL";
    }
    if ([isfrom isEqualToString:@"ccareer"]) {
        lblheader.text = @"FORD";
        lbltitle.text=@"Career";
        groupis = @"FORD";
        subgroupis=@"CAREER";

    }
    if ([isfrom isEqualToString:@"cfeedback"]) {
        lblheader.text = @"FORD";
        lbltitle.text=@"Feedback/Query ";
        groupis = @"FORD";
        subgroupis=@"FEEDBACK_QUERY";
    }
    
    if ([isfrom isEqualToString:@"Inquiry For Site"]) {
        lblheader.text = @"VRAJ";
        lbltitle.text=@"Inquiry For Site";
        groupis = @"VRAJ";
        subgroupis=@"INQUIRY_SITE";
    }
    
    if ([isfrom isEqualToString:@"scareer"]) {
        lblheader.text = @"SUNFLOWER";
        lbltitle.text=@"Career";
        groupis = @"SUNFLOWER";
        subgroupis=@"CAREER_SUNFLOWER";
        
    }
    
    
    if ([tfmsg.text isEqualToString:@""]) {
        tfmsg.text =@"Message";
    }
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(IBAction)btnbackclick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
 
}

-(IBAction)btnsendclick:(id)sender{
    if ([tfname.text isEqualToString:@""]||[tfphone.text isEqualToString:@""]||[tfemail.text isEqualToString:@""]||[tfmsg.text isEqualToString:@"Message"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Please enter all details."
                                                       delegate:self
                                              cancelButtonTitle:@"ok"
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else{
        
        
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.mode = MBProgressHUDModeCustomView;
        HUD.customView = [ApplicationDelegate explosion];
        HUD.color = [UIColor clearColor];
        HUD.dimBackground = YES;
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        [manager setResponseSerializer:responseSerializer];

        
        NSString * api =  [NSString stringWithFormat:@"http://gokulgroups.com/Services/api/InquiryModels"];
        NSDictionary *parameters = @{@"DBID":@0,@"group":groupis,@"sub_group":subgroupis,@"name":tfname.text,@"phone":tfphone.text,@"email":tfemail.text,@"message":tfmsg.text};
        
        [manager POST:api parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             
             [HUD hide:YES];
             tfmsg.text = @"";
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                             message:@"Your inquiry has been submitted successfully. Our representative will contact you back on your provided number.Thank you."
                                                            delegate:self
                                                   cancelButtonTitle:@"ok"
                                                   otherButtonTitles:nil, nil];
             [alert show];
             [self.navigationController popViewControllerAnimated:YES];

             
             
         }
             failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                             message:@"Server down or issue with the internet connection. Please check your connection."
                                                            delegate:self
                                                   cancelButtonTitle:@"ok"
                                                   otherButtonTitles:nil, nil];
             [alert show];
             
             NSLog(@"Error: %@", error);
             [HUD hide:YES];
             
         }];

        
        
    }
}

-(IBAction)btncancelclick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];

}


#pragma mark - UITextViewDelegate
-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    //clear text of textfield when comments text. It should be work like placeholder text
    if([textView.text isEqualToString:@"Message"]){
        textView.text = @"";
    }
}

-(void)setDoneButton
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(canceButtonPressed)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonPressed)]];
    [numberToolbar sizeToFit];
    tfmsg.inputAccessoryView = numberToolbar;
}
-(void)doneButtonPressed
{
    [tfmsg resignFirstResponder];
}
-(void)canceButtonPressed
{
    tfmsg.text=@"Message";
    [tfmsg resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
