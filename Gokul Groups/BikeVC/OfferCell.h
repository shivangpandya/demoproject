//
//  OfferCell.h
//  oogoo
//
//  Created by Shivang on 06/04/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface OfferCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *smallvw;
@property (weak, nonatomic) IBOutlet UIImageView *bgvw;

@end
