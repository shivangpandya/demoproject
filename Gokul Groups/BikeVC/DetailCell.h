//
//  DetailCell.h
//  Gokul Groups
//
//  Created by Shivang on 15/07/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailCell : UITableViewCell

@property(nonatomic)IBOutlet UILabel *lblname;
@property(nonatomic)IBOutlet UILabel *lbldisc;


@end
