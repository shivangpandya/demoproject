//
//  FormVC.h
//  Gokul Groups
//
//  Created by Shivang on 08/06/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "UserDefaultHelper.h"
#import "MBProgressHUD.h"

@interface FormVC : UIViewController{
    
    IBOutlet UILabel *lbltitle;
    IBOutlet UILabel *lblheader;

    
    IBOutlet TPKeyboardAvoidingScrollView*scrlview;
    IBOutlet UITextField *tfname;
    IBOutlet UITextField *tfphone;
    IBOutlet UITextField *tfemail;
    
    IBOutlet UITextView *tfmsg;
    
    IBOutlet UIButton *cancelbtn;
    IBOutlet UIButton *sendbtn;
    
    MBProgressHUD *HUD;

}

@property(nonatomic,strong)NSString *isfrom;
@property(nonatomic,strong)NSString *detailis;

-(IBAction)btnsendclick:(id)sender;
-(IBAction)btnbackclick:(id)sender;
-(IBAction)btncancelclick:(id)sender;


@end
