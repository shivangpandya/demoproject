//
//  ContactUsVC.m
//  Gokul Groups
//
//  Created by Shivang on 27/09/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import "ContactUsVC.h"
#import "OpenInGoogleMapsController.h"

@interface ContactUsVC (){
    
    IBOutlet UIButton*btnmap1;
    IBOutlet UIButton*btnmap2;
    IBOutlet UIScrollView *scrlvw;

    IBOutlet UILabel * mob1;
    IBOutlet UILabel * mob2;
    IBOutlet UILabel * mob3;

    IBOutlet UILabel * mob4;
    IBOutlet UILabel * mob5;


}

@end

@implementation ContactUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationItem setHidesBackButton:YES];
    self.navigationController.navigationBarHidden = YES;
    [self setNeedsStatusBarAppearanceUpdate];

    
    btnmap1.layer.borderColor=[UIColor redColor].CGColor;
    btnmap1.layer.borderWidth=2.0f;
    btnmap1.layer.cornerRadius=3.0f;
    
    btnmap2.layer.borderColor=[UIColor redColor].CGColor;
    btnmap2.layer.borderWidth=2.0f;
    btnmap2.layer.cornerRadius=3.0f;
    
    scrlvw.contentSize = CGSizeMake(0, btnmap2.frame.origin.y+btnmap2.frame.size.height+10);
    
    if (![[OpenInGoogleMapsController sharedInstance] isGoogleMapsInstalled]) {
        
        NSLog(@"Google Maps not installed, but using our fallback strategy");
        [OpenInGoogleMapsController sharedInstance].fallbackStrategy = kGoogleMapsFallbackAppleMaps;
        
    }

    UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnLink:)];
    // if labelView is not set userInteractionEnabled, you must do so
    gesture.numberOfTapsRequired = 1;
    [mob1 setUserInteractionEnabled:YES];
    [mob1 addGestureRecognizer:gesture];
    
    UITapGestureRecognizer* gesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnLink:)];
    // if labelView is not set userInteractionEnabled, you must do so
    gesture.numberOfTapsRequired = 1;
    [mob2 setUserInteractionEnabled:YES];
    [mob2 addGestureRecognizer:gesture1];
    
    UITapGestureRecognizer* gesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnLink:)];
    // if labelView is not set userInteractionEnabled, you must do so
    gesture.numberOfTapsRequired = 1;
    [mob3 setUserInteractionEnabled:YES];
    [mob3 addGestureRecognizer:gesture2];
    
    UITapGestureRecognizer* gesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnLink:)];
    // if labelView is not set userInteractionEnabled, you must do so
    gesture.numberOfTapsRequired = 1;
    [mob4 setUserInteractionEnabled:YES];
    [mob4 addGestureRecognizer:gesture3];
    
    UITapGestureRecognizer* gesture4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTappedOnLink:)];
    // if labelView is not set userInteractionEnabled, you must do so
    gesture.numberOfTapsRequired = 1;
    [mob5 setUserInteractionEnabled:YES];
    [mob5 addGestureRecognizer:gesture4];
    

}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)userTappedOnLink:(UIGestureRecognizer*)gestureRecognizer{
    
    UILabel *label = (UILabel *)gestureRecognizer.view;
    
        if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]])
        {
            if (label.tag == 1) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",mob1.text]]];
            }
            if (label.tag == 2) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",mob2.text]]];
            }
            if (label.tag == 3) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",mob3.text]]];
            }
            if (label.tag == 4) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",mob4.text]]];
            }
            if (label.tag == 5) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",mob5.text]]];
            }
            
        }
        else
        {
            NSLog(@"This device cannot Call");
        }
    
    
    
}
-(IBAction)navigatebtnclick:(id)sender{
    GoogleMapDefinition *defn = [[GoogleMapDefinition alloc] init];
    defn.queryString = [NSString stringWithFormat:@"22.455605 ,70.068348"];
    [[OpenInGoogleMapsController sharedInstance] openMap:defn];
}

-(IBAction)navigatebtn1click:(id)sender{
    
    GoogleMapDefinition *defn = [[GoogleMapDefinition alloc] init];
    defn.queryString = [NSString stringWithFormat:@"22.455605 ,70.068348"];
    [[OpenInGoogleMapsController sharedInstance] openMap:defn];
}

-(IBAction)btnbackclick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
