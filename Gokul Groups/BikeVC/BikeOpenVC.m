//
//  BikeOpenVC.m
//  Gokul Groups
//
//  Created by Shivang on 08/06/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import "BikeOpenVC.h"
#import "OfferCell.h"
#import "AllProductVC.h"
#import "FormVC.h"
#import "ViewController.h"
#import "AboutUsVC.h"
#import "ContactUsVC.h"
@interface BikeOpenVC ()

@end

@implementation BikeOpenVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNeedsStatusBarAppearanceUpdate];

    [OfferCollectionView registerNib:[UINib nibWithNibName:@"OfferCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    OfferCollectionView.backgroundColor = [UIColor clearColor];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(IBAction)backbtnclick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];

}

#pragma mark - UICollectionView Delegate Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 10;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OfferCell *cell = (OfferCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    [cell setBackgroundColor:[UIColor clearColor]];

    if (indexPath.row==0) {
        cell.bgvw.image=[UIImage imageNamed:@"b1"];
    }
    if (indexPath.row==1) {
        cell.bgvw.image=[UIImage imageNamed:@"b2"];
    }if (indexPath.row==2) {
        cell.bgvw.image=[UIImage imageNamed:@"b3"];
    }if (indexPath.row==3) {
        cell.bgvw.image=[UIImage imageNamed:@"b4"];
    }if (indexPath.row==4) {
        cell.bgvw.image=[UIImage imageNamed:@"b5"];
    }if (indexPath.row==5) {
        cell.bgvw.image=[UIImage imageNamed:@"b6"];
    }if (indexPath.row==6) {
        cell.bgvw.image=[UIImage imageNamed:@"b7"];
    }if (indexPath.row==7) {
        cell.bgvw.image=[UIImage imageNamed:@"b8"];
    }if (indexPath.row==8) {
        cell.bgvw.image=[UIImage imageNamed:@"b9"];
    }if (indexPath.row==9) {
        cell.bgvw.image=[UIImage imageNamed:@"b10"];
    }
    return cell;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE_6 || IS_IPHONE_6S)
    {
        return CGSizeMake(186, 186);
    }
    else if (IS_IPHONE_6_PLUS || IS_IPHONE_6S_PLUS)
    {
        return CGSizeMake(206, 206);
    }
    else
    {
        return CGSizeMake(158 , 158);
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        AllProductVC *controller = [[AllProductVC alloc]initWithNibName:@"AllProductVC" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
    }
   else if (indexPath.row==1) {
       FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
       controller.isfrom=@"inquiry";
       controller.detailis=@"";
       [self.navigationController pushViewController:controller animated:YES];
       
   }
    
   else if (indexPath.row==2) {
       FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
       controller.isfrom=@"service";
       [self.navigationController pushViewController:controller animated:YES];
       
   }
   else if (indexPath.row==3) {
       FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
       controller.isfrom=@"parts";
       [self.navigationController pushViewController:controller animated:YES];
   }
   else if (indexPath.row==4) {
       FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
       controller.isfrom=@"exchange";
       [self.navigationController pushViewController:controller animated:YES];
   }
   else if (indexPath.row==5) {
       FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
       controller.isfrom=@"insurance";
       [self.navigationController pushViewController:controller animated:YES];
       
   }
   else if (indexPath.row==6) {
       AboutUsVC *controller = [[AboutUsVC alloc]initWithNibName:@"AboutUsVC" bundle:nil];
       [self.navigationController pushViewController:controller animated:YES];
       
   }
   else if (indexPath.row==7) {
       FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
       controller.isfrom=@"career";
       [self.navigationController pushViewController:controller animated:YES];
       
   }
   else if (indexPath.row==8) {
       ContactUsVC *controller = [[ContactUsVC alloc]initWithNibName:@"ContactUsVC" bundle:nil];
       [self.navigationController pushViewController:controller animated:YES];
       
   }
   else if (indexPath.row==9) {
       FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
       controller.isfrom=@"feedback";
       [self.navigationController pushViewController:controller animated:YES];
       
   }
  
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
