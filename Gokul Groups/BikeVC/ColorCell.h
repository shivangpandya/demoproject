//
//  ColorCell.h
//  Gokul Groups
//
//  Created by Shivang on 10/10/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImage.h"
@interface ColorCell : UITableViewCell
@property(nonatomic)IBOutlet UILabel *lblname;
@property(nonatomic)IBOutlet AsyncImage *img;

@end
