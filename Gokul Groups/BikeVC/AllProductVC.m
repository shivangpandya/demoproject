//
//  AllProductVC.m
//  Gokul Groups
//
//  Created by Shivang on 08/06/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import "AllProductVC.h"
#import "ProductCell.h"
#import "BikeDetailVC.h"
@interface AllProductVC ()

@end

@interface NSDictionary (JRAdditions)
- (NSDictionary *)dictionaryByReplacingNullsWithStrings;
@end

@interface NSArray (NullReplacement)
- (NSArray *)arrayByReplacingNullsWithBlanks;
@end

@implementation NSDictionary (JRAdditions)

- (NSDictionary *)dictionaryByReplacingNullsWithStrings {
    const NSMutableDictionary *replaced = [self mutableCopy];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    
    for (NSString *key in self) {
        id object = [self objectForKey:key];
        if (object == nul) [replaced setObject:blank forKey:key];
        else if ([object isKindOfClass:[NSDictionary class]]) [replaced setObject:[object dictionaryByReplacingNullsWithStrings] forKey:key];
        else if ([object isKindOfClass:[NSArray class]]) [replaced setObject:[object arrayByReplacingNullsWithBlanks] forKey:key];
    }
    return [NSDictionary dictionaryWithDictionary:[replaced copy]];
    
}

@end

@implementation NSArray (NullReplacement)

- (NSArray *)arrayByReplacingNullsWithBlanks  {
    NSMutableArray *replaced = [self mutableCopy];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    for (int idx = 0; idx < [replaced count]; idx++) {
        id object = [replaced objectAtIndex:idx];
        if (object == nul) [replaced replaceObjectAtIndex:idx withObject:blank];
        else if ([object isKindOfClass:[NSDictionary class]]) [replaced replaceObjectAtIndex:idx withObject:[object dictionaryByReplacingNullsWithStrings]];
        else if ([object isKindOfClass:[NSArray class]]) [replaced replaceObjectAtIndex:idx withObject:[object arrayByReplacingNullsWithBlanks]];
    }
    return [replaced copy];
}

@end

@implementation AllProductVC


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNeedsStatusBarAppearanceUpdate];

    
    [CategoryCollectionView registerNib:[UINib nibWithNibName:@"ProductCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    CategoryCollectionView.backgroundColor = [UIColor clearColor];
    
    [self Getevent];
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(IBAction)backbtnclick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)Getevent
{
    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.mode = MBProgressHUDModeCustomView;
    HUD.customView = [ApplicationDelegate explosion];
    HUD.color = [UIColor clearColor];
    HUD.dimBackground = YES;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager setResponseSerializer:responseSerializer];
    //[manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //manager.requestSerializer = [AFJSONRequestSerializer serializer];


    NSString * api =  [NSString stringWithFormat:@"http://gokulgroups.com/Services/api/BikeModels"];
    
    
    [manager GET:api parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         arrResponse=[[NSMutableArray alloc] init];
         arrResponse=[[responseObject arrayByReplacingNullsWithBlanks] mutableCopy];
         
         [CategoryCollectionView reloadData];
         [HUD hide:YES];
         
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                         message:@"Server down or issue with the internet connection. Please check your connection."
                                                        delegate:self
                                               cancelButtonTitle:@"ok"
                                               otherButtonTitles:nil, nil];
         [alert show];
         
         NSLog(@"Error: %@", error);
         [HUD hide:YES];
         
     }];
    
}



#pragma mark - UICollectionView Delegate Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrResponse.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductCell *cell = (ProductCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.lblname.text =[[arrResponse valueForKey:@"bikeTitle"] objectAtIndex:indexPath.row];
    
    if ([[[arrResponse valueForKey:@"imageUrl"]objectAtIndex:indexPath.row]isKindOfClass:[NSString class]])  {
        
        NSLog(@"Image is %@",[[arrResponse valueForKey:@"imageUrl"]objectAtIndex:indexPath.row] );
        [cell.pic setImageWithURL:[[arrResponse valueForKey:@"imageUrl"]objectAtIndex:indexPath.row] placeholderImage:[UIImage imageNamed:@"Bike_ic"]];
        
    }
    else{
        cell.pic.image=[UIImage imageNamed:@"Bike_ic"];
    }
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE_6 || IS_IPHONE_6S)
    {
        return CGSizeMake(186, 186);
    }
    else if (IS_IPHONE_6_PLUS || IS_IPHONE_6S_PLUS)
    {
        return CGSizeMake(206, 206);
    }
    else
    {
        return CGSizeMake(158 , 158);
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //BikeDetailVC
    
    BikeDetailVC *controller = [[BikeDetailVC alloc]initWithNibName:@"BikeDetailVC" bundle:nil];
    controller.arrdetail=[[arrResponse objectAtIndex:indexPath.row] mutableCopy];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
