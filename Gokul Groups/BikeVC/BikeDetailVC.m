//
//  BikeDetailVC.m
//  Gokul Groups
//
//  Created by Shivang on 15/07/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import "BikeDetailVC.h"
#import "DetailCell.h"
#import "ColorCell.h"
#import "Constants.h"
#import "FormVC.h"
@interface BikeDetailVC (){
    IBOutlet UIButton *cancelbtn;
    IBOutlet UIButton *sendbtn;
    IBOutlet UIView *bottomvw;

}

@end

@implementation BikeDetailVC
@synthesize arrdetail;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNeedsStatusBarAppearanceUpdate];

    sendbtn.layer.borderWidth=2.0f;
    sendbtn.layer.borderColor=[UIColor redColor].CGColor;
    sendbtn.layer.cornerRadius=3.0f;
    cancelbtn.layer.borderWidth=2.0f;
    cancelbtn.layer.borderColor=[UIColor redColor].CGColor;
    cancelbtn.layer.cornerRadius=3.0f;
    
    if ([[arrdetail valueForKey:@"imageUrl"]isKindOfClass:[NSString class]])  {
        NSLog(@"Image is %@",[arrdetail valueForKey:@"imageUrl"] );
        [pic setImageWithURL:[arrdetail valueForKey:@"imageUrl"]placeholderImage:[UIImage imageNamed:@"Bike_ic"]];
        
    }
    else{
        pic.image=[UIImage imageNamed:@"Bike_ic"];
    }
    
    lblname.text = [arrdetail valueForKey:@"bikeTitle"];
    
     [tblviewC registerNib:[UINib nibWithNibName:@"ColorCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    NSMutableArray * arr = [arrdetail valueForKey:@"colorImageUrl"];
    colorary = [[NSMutableArray alloc] init];
    colorary1 = [[NSMutableArray alloc]init];
    for (int i=0; i<arr.count; i++) {
        NSString * str = [[arr valueForKey:@"Description"] objectAtIndex:i];
        [colorary addObject:str];
        NSString * str1 = [[arr valueForKey:@"ImagePath"] objectAtIndex:i];
        [colorary1 addObject:str1];
        
    }
    
    
    
   // colorary =[arrdetail valueForKey:@"colorNames"];
    
    
    [tblviewE registerNib:[UINib nibWithNibName:@"DetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    engineary =[[NSMutableArray alloc] initWithObjects:@"Type",@"Maximum Power",@"Maximum Torque",@"Ignition",@"Starting",@"Displacement", nil];
    engineary1 =[[NSMutableArray alloc] initWithObjects:[arrdetail valueForKey:@"esType"],[arrdetail valueForKey:@"esMaxPower"],[arrdetail valueForKey:@"esMaxTorque"],[arrdetail valueForKey:@"esIgnition"],[arrdetail valueForKey:@"esStarting"],[arrdetail valueForKey:@"esDisplacement"],nil];

   
    
    [tblviewD registerNib:[UINib nibWithNibName:@"DetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    diary =[[NSMutableArray alloc] initWithObjects:@"Length",@"Width",@"Height",@"Ground Clearance",@"Wheelbase", nil];
    diary1 =[[NSMutableArray alloc] initWithObjects:[arrdetail valueForKey:@"dimLength"],[arrdetail valueForKey:@"dimWidth"],[arrdetail valueForKey:@"dimHeight"],[arrdetail valueForKey:@"dimGroundClearance"],[arrdetail valueForKey:@"dimWheelbase"], nil];
    
    
    [tblviewW registerNib:[UINib nibWithNibName:@"DetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    wheelary =[[NSMutableArray alloc] initWithObjects:@"Rim Size Front",@"Rim Size Rear",@"Tyre Size Front",@"Tyre Size Rear",@"Brakes Front",@"Brakes Rear", nil];
    wheelary1 =[[NSMutableArray alloc] initWithObjects:[arrdetail valueForKey:@"wtRimSizeFront"],[arrdetail valueForKey:@"wtRimSizeRear"],[arrdetail valueForKey:@"wtTyreSizeFront"],[arrdetail valueForKey:@"wtTyreSizeRear"],[arrdetail valueForKey:@"wtBrakesFront"],[arrdetail valueForKey:@"wtBrakesRear"], nil];
   
    
    [tblviewT registerNib:[UINib nibWithNibName:@"DetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    transary =[[NSMutableArray alloc] initWithObjects:@"Clutch",@"Gear Box", nil];
    transary1 =[[NSMutableArray alloc] initWithObjects:[arrdetail valueForKey:@"transClutch"],[arrdetail valueForKey:@"transGearBox"], nil];

    
    
    [tblviewCh registerNib:[UINib nibWithNibName:@"DetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    chesary =[[NSMutableArray alloc] initWithObjects:@"Type", nil];
    chesary1 =[[NSMutableArray alloc] initWithObjects:[arrdetail valueForKey:@"ChassisType"], nil];

   
    
    [tblviewS registerNib:[UINib nibWithNibName:@"DetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    susary =[[NSMutableArray alloc] initWithObjects:@"Front",@"Rear", nil];
    susary1 =[[NSMutableArray alloc] initWithObjects:[arrdetail valueForKey:@"SuspensionsFront"],[arrdetail valueForKey:@"SuspensionsRear"], nil];

   
    
    
    [tblviewEle registerNib:[UINib nibWithNibName:@"DetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    eleary =[[NSMutableArray alloc] initWithObjects:@"Battery",@"Head Lamps",@"Tail/Stop Lamps", nil];
    eleary1 =[[NSMutableArray alloc] initWithObjects:[arrdetail valueForKey:@"elecBattery"],[arrdetail valueForKey:@"elecHeadLamp"],[arrdetail valueForKey:@"elecTailLamp"], nil];

   
    
    [tblviewWeight registerNib:[UINib nibWithNibName:@"DetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    weightary =[[NSMutableArray alloc] initWithObjects:@"Kerb Weight",@"Max Payload",@"Fuel Tank Capacity",@"Reserve", nil];
    weightary1 =[[NSMutableArray alloc] initWithObjects:[arrdetail valueForKey:@"weightKerWeight"],[arrdetail valueForKey:@"weightMaxPayLoad"],[arrdetail valueForKey:@"weightFuelTankCapacity"],[arrdetail valueForKey:@"weightReserve"], nil];

    
    
    
    
    
    
}

-(void)viewDidLayoutSubviews{
    
    [tblviewC reloadData];
    [tblviewE reloadData];
    [tblviewD reloadData];
    [tblviewW reloadData];
    [tblviewT reloadData];
    [tblviewCh reloadData];
    [tblviewS reloadData];
    [tblviewEle reloadData];
    [tblviewWeight reloadData];
    
    tblviewC.frame = CGRectMake(5, 135, [UIScreen mainScreen].bounds.size.width-10, tblviewC.contentSize.height);
    tblviewC.layer.borderColor=[UIColor grayColor].CGColor;
    tblviewC.layer.borderWidth=2.0f;
    tblviewC.tableHeaderView=hview;
    [scrlvw addSubview:tblviewC];
    
    tblviewE.frame = CGRectMake(5, tblviewC.frame.origin.y+tblviewC.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, tblviewE.contentSize.height);
    tblviewE.layer.borderColor=[UIColor grayColor].CGColor;
    tblviewE.layer.borderWidth=2.0f;
    tblviewE.tableHeaderView=hview1;
    [scrlvw addSubview:tblviewE];
    
    tblviewD.frame = CGRectMake(5, tblviewE.frame.origin.y+tblviewE.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, tblviewD.contentSize.height);
    tblviewD.layer.borderColor=[UIColor grayColor].CGColor;
    tblviewD.layer.borderWidth=2.0f;
    tblviewD.tableHeaderView=hview2;
    [scrlvw addSubview:tblviewD];
    
    tblviewW.frame = CGRectMake(5, tblviewD.frame.origin.y+tblviewD.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, tblviewW.contentSize.height);
    tblviewW.layer.borderColor=[UIColor grayColor].CGColor;
    tblviewW.layer.borderWidth=2.0f;
    tblviewW.tableHeaderView=hview3;
    [scrlvw addSubview:tblviewW];
    
    tblviewT.frame = CGRectMake(5, tblviewW.frame.origin.y+tblviewW.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, tblviewT.contentSize.height);
    tblviewT.layer.borderColor=[UIColor grayColor].CGColor;
    tblviewT.layer.borderWidth=2.0f;
    tblviewT.tableHeaderView=hview4;
    [scrlvw addSubview:tblviewT];
    
    tblviewCh.frame = CGRectMake(5, tblviewT.frame.origin.y+tblviewT.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, tblviewCh.contentSize.height);
    tblviewCh.layer.borderColor=[UIColor grayColor].CGColor;
    tblviewCh.layer.borderWidth=2.0f;
    tblviewCh.tableHeaderView=hview5;
    [scrlvw addSubview:tblviewCh];
    
    tblviewS.frame = CGRectMake(5, tblviewCh.frame.origin.y+tblviewCh.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, tblviewS.contentSize.height);
    tblviewS.layer.borderColor=[UIColor grayColor].CGColor;
    tblviewS.layer.borderWidth=2.0f;
    tblviewS.tableHeaderView=hview6;
    [scrlvw addSubview:tblviewS];
    
    tblviewEle.frame = CGRectMake(5, tblviewS.frame.origin.y+tblviewS.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, tblviewEle.contentSize.height);
    tblviewEle.layer.borderColor=[UIColor grayColor].CGColor;
    tblviewEle.layer.borderWidth=2.0f;
    tblviewEle.tableHeaderView=hview7;
    [scrlvw addSubview:tblviewEle];
    
    tblviewWeight.frame = CGRectMake(5, tblviewEle.frame.origin.y+tblviewEle.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, tblviewWeight.contentSize.height);
    tblviewWeight.layer.borderColor=[UIColor grayColor].CGColor;
    tblviewWeight.layer.borderWidth=2.0f;
    tblviewWeight.tableHeaderView=hview8;
    [scrlvw addSubview:tblviewWeight];
    
    bottomvw.frame = CGRectMake(5, tblviewWeight.frame.origin.y+tblviewWeight.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, bottomvw.frame.size.height);
    [scrlvw addSubview:bottomvw];
    scrlvw.contentSize = CGSizeMake(0, bottomvw.frame.origin.y+bottomvw.frame.size.height+10);
    
   
    
     space=165;
    
}

-(IBAction)backbtnclick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==tblviewC) {
        return colorary.count;
    }
    if (tableView==tblviewE) {
        return engineary.count;

    }
    if (tableView==tblviewD) {
        return diary.count;

    }
    if (tableView==tblviewW) {
        return wheelary.count;

    }
    if (tableView==tblviewT) {
        return transary.count;

    }
    if (tableView==tblviewCh) {
        return chesary.count;

    }
    if (tableView==tblviewS) {
        return susary.count;

    }
    if (tableView==tblviewEle) {
        return eleary.count;

    }
   else  {
        return weightary.count;

    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (tableView==tblviewC) {
        return 60;
    }
    else{
    
    int topPadding = 13;
    int bottomPadding = 13;
    
    NSString *text;
    
     if (tableView==tblviewE) {
    text = [engineary1 objectAtIndex:[indexPath row]];
     }
    
    
    if (tableView==tblviewD) {
        text = [diary1 objectAtIndex:[indexPath row]];
    }
    if (tableView==tblviewW) {
        text = [wheelary1 objectAtIndex:[indexPath row]];
        
    }
    if (tableView==tblviewT) {
        text = [transary1 objectAtIndex:[indexPath row]];
        
    }
    if (tableView==tblviewCh) {
        text = [chesary1 objectAtIndex:[indexPath row]];
        
    }
    if (tableView==tblviewS) {
        text = [susary1 objectAtIndex:[indexPath row]];
        
    }
    if (tableView==tblviewEle) {
        text = [eleary1 objectAtIndex:[indexPath row]];
        
    }
    if (tableView==tblviewWeight) {
        text = [weightary1 objectAtIndex:[indexPath row]];
        
    }
    
    CGRect textRect = [text boundingRectWithSize:
                       CGSizeMake([UIScreen mainScreen].bounds.size.width-space,CGFLOAT_MAX)
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName:REGULAR_FONT(12)}
                                         context:nil];
    
    if (textRect.size.height+26 > 44) {
        NSLog(@"lbl origin : %f",topPadding+textRect.size.height+bottomPadding);

             return topPadding+textRect.size.height+bottomPadding;

         }
         else{
            return 44;
         }
    
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView==tblviewC) {
        
        static NSString *CellIdentifier = @"cell";
        
        ColorCell* cell =  (ColorCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.lblname.text =[colorary objectAtIndex:indexPath.row];
        
        [cell.img setImageWithURL:[colorary1 objectAtIndex:indexPath.row] placeholderImage:[UIImage imageNamed:@"Bike_ic"]];

       return cell;
    }
    
    else{
        
    
    static NSString *CellIdentifier = @"cell";
    
    DetailCell* cell =  (DetailCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
   
    if (tableView==tblviewE) {
        
        cell.lblname.text =[engineary objectAtIndex:indexPath.row];
        
        CGRect textRect = [[engineary1 objectAtIndex:[indexPath row]] boundingRectWithSize:
                           CGSizeMake([UIScreen mainScreen].bounds.size.width-space,CGFLOAT_MAX)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{NSFontAttributeName:REGULAR_FONT(12)}
                                             context:nil];
        
        cell.lbldisc.frame=CGRectMake(150, 13, [UIScreen mainScreen].bounds.size.width-space, textRect.size.height);
        
        NSLog(@"height : %f  height2 : %f",textRect.size.height ,cell.lbldisc.frame.size.height );

        cell.lbldisc.text =[engineary1 objectAtIndex:indexPath.row];
        
    }
    if (tableView==tblviewD) {
        cell.lblname.text =[diary objectAtIndex:indexPath.row];
        CGRect textRect = [[diary1 objectAtIndex:[indexPath row]] boundingRectWithSize:
                           CGSizeMake([UIScreen mainScreen].bounds.size.width-space,CGFLOAT_MAX)
                                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                                attributes:@{NSFontAttributeName:REGULAR_FONT(12)}
                                                                                   context:nil];
        
        cell.lbldisc.frame=CGRectMake(150, 13, [UIScreen mainScreen].bounds.size.width-space, textRect.size.height);
        
        cell.lbldisc.text =[diary1 objectAtIndex:indexPath.row];
    }
    if (tableView==tblviewW) {
        cell.lblname.text =[wheelary objectAtIndex:indexPath.row];
        CGRect textRect = [[wheelary1 objectAtIndex:[indexPath row]] boundingRectWithSize:
                           CGSizeMake([UIScreen mainScreen].bounds.size.width-space,CGFLOAT_MAX)
                                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                                attributes:@{NSFontAttributeName:REGULAR_FONT(12)}
                                                                                   context:nil];
        
        cell.lbldisc.frame=CGRectMake(150, 13, [UIScreen mainScreen].bounds.size.width-space, textRect.size.height);
        
        cell.lbldisc.text =[wheelary1 objectAtIndex:indexPath.row];
    }
    if (tableView==tblviewT) {
        cell.lblname.text =[transary objectAtIndex:indexPath.row];
        CGRect textRect = [[transary1 objectAtIndex:[indexPath row]] boundingRectWithSize:
                           CGSizeMake([UIScreen mainScreen].bounds.size.width-space,CGFLOAT_MAX)
                                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                                attributes:@{NSFontAttributeName:REGULAR_FONT(12)}
                                                                                   context:nil];
        
        cell.lbldisc.frame=CGRectMake(150, 13, [UIScreen mainScreen].bounds.size.width-space, textRect.size.height);
        
        cell.lbldisc.text =[transary1 objectAtIndex:indexPath.row];
    }
    if (tableView==tblviewCh) {
        cell.lblname.text =[chesary objectAtIndex:indexPath.row];
        CGRect textRect = [[chesary1 objectAtIndex:[indexPath row]] boundingRectWithSize:
                           CGSizeMake([UIScreen mainScreen].bounds.size.width-space,CGFLOAT_MAX)
                                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                                attributes:@{NSFontAttributeName:REGULAR_FONT(12)}
                                                                                   context:nil];
        
        cell.lbldisc.frame=CGRectMake(150, 13, [UIScreen mainScreen].bounds.size.width-space, textRect.size.height);
        
        cell.lbldisc.text =[chesary1 objectAtIndex:indexPath.row];
    }
    if (tableView==tblviewS) {
        cell.lblname.text =[susary objectAtIndex:indexPath.row];
        CGRect textRect = [[susary1 objectAtIndex:[indexPath row]] boundingRectWithSize:
                           CGSizeMake([UIScreen mainScreen].bounds.size.width-space,CGFLOAT_MAX)
                                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                                attributes:@{NSFontAttributeName:REGULAR_FONT(12)}
                                                                                   context:nil];
        
        cell.lbldisc.frame=CGRectMake(150, 13, [UIScreen mainScreen].bounds.size.width-space, textRect.size.height);
        
        cell.lbldisc.text =[susary1 objectAtIndex:indexPath.row];
    }
    if (tableView==tblviewEle) {
        cell.lblname.text =[eleary objectAtIndex:indexPath.row];
        CGRect textRect = [[eleary1 objectAtIndex:[indexPath row]] boundingRectWithSize:
                           CGSizeMake([UIScreen mainScreen].bounds.size.width-space,CGFLOAT_MAX)
                                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                                attributes:@{NSFontAttributeName:REGULAR_FONT(12)}
                                                                                   context:nil];
        
        cell.lbldisc.frame=CGRectMake(150, 13, [UIScreen mainScreen].bounds.size.width-space, textRect.size.height);
        
        cell.lbldisc.text =[eleary1 objectAtIndex:indexPath.row];
    }
    if (tableView==tblviewWeight) {
        cell.lblname.text =[weightary objectAtIndex:indexPath.row];
        CGRect textRect = [[weightary1 objectAtIndex:[indexPath row]] boundingRectWithSize:
                           CGSizeMake([UIScreen mainScreen].bounds.size.width-space,CGFLOAT_MAX)
                                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                                attributes:@{NSFontAttributeName:REGULAR_FONT(12)}
                                                                                   context:nil];
        
        cell.lbldisc.frame=CGRectMake(150, 13, [UIScreen mainScreen].bounds.size.width-space, textRect.size.height);
        
        cell.lbldisc.text =[weightary1 objectAtIndex:indexPath.row];
    }
    
    return cell;
        
    }

}


-(IBAction)btncancelclick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)btnsendclick:(id)sender{
    
    FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
    controller.isfrom=@"inquiry";
    controller.detailis =[NSString stringWithFormat:@"Inquiry for %@",[arrdetail valueForKey:@"bikeTitle"]] ;
    [self.navigationController pushViewController:controller animated:YES];

}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
