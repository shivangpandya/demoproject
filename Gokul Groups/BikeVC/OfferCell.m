//
//  OfferCell.m
//  oogoo
//
//  Created by Shivang on 06/04/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import "OfferCell.h"

@implementation OfferCell
@synthesize bgvw;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    if (IS_IPHONE_6 || IS_IPHONE_6S)
    {
      bgvw.frame = CGRectMake(0,0,150,150);
        bgvw.center=self.center;

    }
    else if (IS_IPHONE_6_PLUS || IS_IPHONE_6S_PLUS)
    {
      bgvw.frame = CGRectMake(0,0,165,165);
        bgvw.center=self.center;

    }
    else
    {
        bgvw.frame = CGRectMake(0,0,120,120);
        bgvw.center=self.center;
    }
    
    bgvw.layer.shadowOffset = CGSizeMake(0, 0);
    bgvw.layer.shadowRadius = 6;
    bgvw.layer.shadowColor = [UIColor blackColor].CGColor;
    bgvw.layer.shadowOpacity = 0.3;
    bgvw.backgroundColor=[UIColor clearColor];

    
   // bgvw.layer.cornerRadius=6.0f;
  //  bgvw.layer.masksToBounds = YES;
    
    
}

@end
