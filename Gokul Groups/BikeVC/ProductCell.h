//
//  ProductCell.h
//  Gokul Groups
//
//  Created by Shivang on 08/06/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImage.h"


@interface ProductCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblname;
@property (weak, nonatomic) IBOutlet AsyncImage *pic;

@end
