//
//  MainVC.m
//  Gokul Groups
//
//  Created by Shivang on 08/06/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import "MainVC.h"
#import "BikeOpenVC.h"
#import "CarOpenVC.h"
#import "VirajOpenVC.h"
#import "SchoolOpenVC.h"
@interface MainVC ()

@end

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNeedsStatusBarAppearanceUpdate];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationController.navigationBarHidden = YES;
    
    bikevw.layer.cornerRadius = 6;
    bikevw.layer.shadowOffset = CGSizeMake(0, 0);
    bikevw.layer.shadowRadius = 2;
    bikevw.layer.shadowColor = [UIColor blackColor].CGColor;
    bikevw.layer.shadowOpacity = 0.3;

    carvw.layer.cornerRadius = 6;
    carvw.layer.shadowOffset = CGSizeMake(0, 0);
    carvw.layer.shadowRadius = 2;
    carvw.layer.shadowColor = [UIColor blackColor].CGColor;
    carvw.layer.shadowOpacity = 0.3;
    
    sclvw.layer.cornerRadius = 6;
    sclvw.layer.shadowOffset = CGSizeMake(0, 0);
    sclvw.layer.shadowRadius = 2;
    sclvw.layer.shadowColor = [UIColor blackColor].CGColor;
    sclvw.layer.shadowOpacity = 0.3;
    
    virajvw.layer.cornerRadius = 6;
    virajvw.layer.shadowOffset = CGSizeMake(0, 0);
    virajvw.layer.shadowRadius = 2;
    virajvw.layer.shadowColor = [UIColor blackColor].CGColor;
    virajvw.layer.shadowOpacity = 0.3;
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(IBAction)bikebtnclick:(id)sender{
    
    BikeOpenVC *controller = [[BikeOpenVC alloc]initWithNibName:@"BikeOpenVC" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}

-(IBAction)carbtnclick:(id)sender{
    CarOpenVC *controller = [[CarOpenVC alloc]initWithNibName:@"CarOpenVC" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];    
}
-(IBAction)virajbtnclick:(id)sender{
    VirajOpenVC *controller = [[VirajOpenVC alloc]initWithNibName:@"VirajOpenVC" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
    
}
-(IBAction)schoolbtnclick:(id)sender{
    SchoolOpenVC *controller = [[SchoolOpenVC alloc]initWithNibName:@"SchoolOpenVC" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
