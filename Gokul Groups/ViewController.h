//
//  ViewController.h
//  Gokul Groups
//
//  Created by Shivang on 07/06/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
@interface ViewController : UIViewController{
    
   
    IBOutlet TPKeyboardAvoidingScrollView*scrlview;
    
    IBOutlet UITextField *tfname;
    IBOutlet UITextField *tfphone;
    IBOutlet UITextField *tfemail;
    IBOutlet UITextField *tfreg;
    
    IBOutlet UIView *hidevw;
    
    IBOutlet UISwitch *onswitch;
    
    IBOutlet UIButton*skipbtn;
    IBOutlet UIButton*regbtn;
    
    IBOutlet UIView *downvw;
    
}

-(IBAction)btnskipclick:(id)sender;
-(IBAction)btnregisterclick:(id)sender;
@end

