//
//  UserDefaultHelper.h
//  Tinder
//
//  Created by Elluminati - macbook on 10/04/14.
//  Copyright (c) 2014 AppDupe. All rights reserved.
//

#import <Foundation/Foundation.h>

#define USERDEFAULT [NSUserDefaults standardUserDefaults]

//UserDefault Keys
extern NSString *const UD_USER_INFO;//UserInfo
extern NSString *const UD_DEVICETOKEN;//DeviceToken



@interface UserDefaultHelper : NSObject
{
    NSMutableDictionary *userInfo;
    NSString *deviceToken;
    
   
    
}

-(id)init;
+(UserDefaultHelper *)sharedObject;

//getter
-(NSMutableDictionary *)userInfo;

-(NSString *)deviceToken;




//setter
-(void)setUserInfo:(NSMutableDictionary *)newUserInfo;
-(void)setDeviceToken:(NSString *)newDeviceToken;


-(void)clearAll;

@end
