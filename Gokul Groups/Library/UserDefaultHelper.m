//
//  UserDefaultHelper.m
//  Tinder
//
//  Created by Elluminati - macbook on 10/04/14.
//  Copyright (c) 2014 AppDupe. All rights reserved.
//


#import "UserDefaultHelper.h"

//UserDefault Keys
NSString *const UD_USER_INFO=@"UserInfo";
NSString *const UD_DEVICETOKEN=@"DeviceToken";

@implementation UserDefaultHelper

#pragma mark -
#pragma mark - Init

-(id)init
{
    if((self = [super init]))
    {
        [self setAllData];
    }
    return self;
}

+(UserDefaultHelper *)sharedObject
{
    static UserDefaultHelper *obj = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        obj = [[UserDefaultHelper alloc] init];
    });
    return obj;
}

#pragma mark -
#pragma mark - SetAllData

-(void)setAllData
{
  
}

#pragma mark -
#pragma mark - Getter

-(NSMutableDictionary *)userInfo{
    userInfo=[USERDEFAULT objectForKey:UD_USER_INFO];
    return userInfo;
}


-(NSString *)deviceToken
{
    deviceToken=[USERDEFAULT objectForKey:UD_DEVICETOKEN];
    if (deviceToken==nil) {
        deviceToken=@"IOS_SIMULATOR";
    }
    return deviceToken;
}


-(void)clearAll{
    [USERDEFAULT removeObjectForKey:UD_USER_INFO];
    
    [USERDEFAULT synchronize];
}

-(void)setUserInfo:(NSMutableDictionary *)newUserInfo
{
    userInfo=newUserInfo;
    [USERDEFAULT setObject:userInfo forKey:UD_USER_INFO];
    [USERDEFAULT synchronize];
}

-(void)setDeviceToken:(NSString *)newDeviceToken
{
    deviceToken=newDeviceToken;
    [USERDEFAULT setObject:deviceToken forKey:UD_DEVICETOKEN];
    [USERDEFAULT synchronize];
}


@end
