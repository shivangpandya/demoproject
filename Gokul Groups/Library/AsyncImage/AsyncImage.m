

#import "AsyncImage.h"
#import "FullyLoaded.h"

@interface AsyncImage()
{
    UIActivityIndicatorView *_indicatorView;
    UIView *_loadingView;
}
@end


@implementation AsyncImage
@synthesize animationType;
@synthesize asyncImageDelegate = _asyncImageDelegate;
@synthesize tag1 = _tag1;
@synthesize placeholderImage = _placeholderImage;
@synthesize needIndicator = _needIndicator,indicatorViewStyle = _indicatorViewStyle;
@synthesize isDownloaded = _isDownloaded;

static const float kDuration = 1.0;
static const float kAnimationChangeDuration = 2.0;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        _indicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame andIndicatorStyle:(UIActivityIndicatorViewStyle)indicatorStyle
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        _indicatorViewStyle = indicatorStyle;
        [self addIndicatorView];
    }
    return self;
}
//-(void)setFrame:(CGRect)frame
//{
//    [super setFrame:frame];
//    if (_loadingView) {
//        _loadingView.center = CGPointMake(self.frame.size.width-22, 65+22);
//
//    }
//}
#pragma mark
#pragma mark - IndicatorView Methdos 

-(void)addIndicatorView
{
    if (!_loadingView)
    {
        _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 22, 22)];
        [_loadingView setBackgroundColor:[UIColor blackColor]];
        _loadingView.center = self.center;
        _loadingView.layer.borderWidth = 0.5;
        _loadingView.layer.borderColor = [[UIColor blackColor] CGColor];
        _loadingView.layer.cornerRadius = 22/2;
        _loadingView.layer.masksToBounds = YES;
        
        _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:_indicatorViewStyle];
        [_indicatorView setHidesWhenStopped:YES];
        [_loadingView addSubview:_indicatorView];

        [self.superview addSubview:_loadingView];
        
        [_loadingView setHidden:YES];

    }
}

-(void)removeIndicatorView
{
    if (_loadingView)
    {
        [_indicatorView stopAnimating];
        [_indicatorView removeFromSuperview];
        [_loadingView removeFromSuperview];
    }
}
-(BOOL)needIndicator
{
    return _needIndicator;
}

-(void)setNeedIndicator:(BOOL)needIndicator
{
    if (needIndicator != _needIndicator)
    {
        _needIndicator = needIndicator;
    }
    
    if (_needIndicator)
    {
        [self addIndicatorView];
    }
    else
    {
        [self removeIndicatorView];
    }
}

-(UIActivityIndicatorViewStyle)indicatorViewStyle
{
    return _indicatorViewStyle;
}

-(void)setIndicatorViewStyle:(UIActivityIndicatorViewStyle)indicatorViewStyle
{
    if (indicatorViewStyle != _indicatorViewStyle)
    {
        _indicatorViewStyle = indicatorViewStyle;
    }
    
}

-(void)startAnimateIndicator
{
    if (_needIndicator)
    {
        [_loadingView setHidden:NO];
        _loadingView.center = CGPointMake(self.frame.size.width-22, 65+22);
        [_indicatorView startAnimating];
    }
}

-(void)stopAnimateIndicator
{
    if (_needIndicator)
    {
        [_indicatorView stopAnimating];
        [_loadingView setHidden:YES];
    }
}
#pragma mark
#pragma mark - Set content mode type
-(void)setContentMode:(UIViewContentMode)contentMode
{
    [super setContentMode:contentMode];
    _contentModeType = contentMode;
}

-(void)setModeForImage:(UIViewContentMode)contentMode
{
    [super setContentMode:contentMode];
}

#pragma mark
#pragma mark  - SET URL FOR IMAGE WITH PLACEHOLDER IMAGE
-(void)setImageWithURL:(NSString*)strURL placeholderImage:(UIImage*)placeholdImage
{
    _isDownloaded = NO;
    strCurrUrl =  strURL;       //Store current Url
    
    [[FullyLoaded sharedFullyLoaded]removeCachedImageForURL:strURL];


    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
    
        @autoreleasepool {
            UIImage *image = [[FullyLoaded sharedFullyLoaded] imageForURL:strURL];
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                @autoreleasepool {
                    
                    if (image)
                    {
                        self.image = image;
                        
                        [self receivedImage:image];
                        [self setModeForImage:_contentModeType];
                    }
                    else
                    {
                        if (placeholdImage)
                        {
                            self.image = placeholdImage;//Set placeholder image to self(UIImageView)
                            
                            if (placeholdImage.size.width > self.frame.size.width || placeholdImage.size.height > self.frame.size.height) {
                                
                                [self setModeForImage:UIViewContentModeScaleAspectFill];
                            }
                            else
                            {
                                [self setModeForImage:UIViewContentModeCenter];
                            }
                        }
                        [self requestForImage:strURL];
                    }
                }
            });
        }
        
       
    });
}

#pragma mark
#pragma mark - Pass array of Url in Album
-(void)setImagesArrayFromURL:(NSMutableArray*)arrStrURL withPlaceholderImage:(UIImage*)placeholdImage {
    
    if ([arrStrURL count] > 0) {
        
        if (!self.image) {
            if (placeholdImage) {
                self.image = placeholdImage;
            }
        }
        [timerAnim invalidate];
        timerAnim = nil;
        timerAnim = [NSTimer scheduledTimerWithTimeInterval:kAnimationChangeDuration target:self selector:@selector(setImageInAlbum:) userInfo:arrStrURL repeats:YES];
    }else{
        if (placeholdImage) {
            self.image = placeholdImage;
        }
    }
}
#pragma mark
#pragma mark -  Set image Url in Album Cover
-(void)setImageInAlbum:(NSTimer *)timer {
    
    @try {
        NSUInteger randomIndex = arc4random() % [timer.userInfo count];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            UIImage *image = [[FullyLoaded sharedFullyLoaded] imageForURL:[timer.userInfo objectAtIndex:randomIndex]];
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                if (image)
                {
                    [UIView transitionWithView:self
                                      duration:kDuration
                                       options: animationType|UIViewAnimationOptionAllowUserInteraction
                                    animations:^{
                                        
                                        [self setModeForImage:_contentModeType];
                                        self.image = image;
                                        [self performSelectorOnMainThread:@selector(receivedImage:) withObject:image waitUntilDone:NO];
                                    }
                                    completion:nil];
                }
                else
                {
                    [self requestForImage:[timer.userInfo objectAtIndex:randomIndex]];
                }
            });
        });
    }
    @catch (NSException *exception) {
        NSLog(@"%s Exception:%@",__PRETTY_FUNCTION__,exception);
    }
    @finally {
    }
}
#pragma mark - Stop Album Image Animation -
-(void)stopAnimation
{
    [[NSRunLoop currentRunLoop] cancelPerformSelectorsWithTarget:self];
    [[NSRunLoop mainRunLoop]cancelPerformSelectorsWithTarget:self];
    if ([timerAnim isValid]) {
        [timerAnim invalidate];
        timerAnim = nil;
    }
}
#pragma mark
#pragma mark - REQUEST FOR IMAGE URL
-(void)requestForImage:(NSString*)strURL{
    
    [self performSelectorOnMainThread:@selector(startAnimateIndicator) withObject:nil waitUntilDone:NO];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    NSURLRequest *request;
    NSString * newImageURL = [strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    request = [NSURLRequest requestWithURL:[NSURL URLWithString:newImageURL]];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         UIImage *image = [UIImage imageWithData:data];
         
         if ([data length] > 0 && error == nil && image)  //If  success
         {
             [[FullyLoaded sharedFullyLoaded] setImage:image withKey:strURL];
             if ([strURL isEqualToString:strCurrUrl]) {
                 
                 //recive data
                 
                 dispatch_queue_t queue_ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                 dispatch_async(queue_, ^{
                     
                     
                     UIImage *image = [[FullyLoaded sharedFullyLoaded] imageForURL:strURL];
                     
                     [self performSelectorOnMainThread:@selector(receivedImage:) withObject:image waitUntilDone:NO];
                     
                     [data writeToFile:[[NSUserDefaults standardUserDefaults] valueForKey:strURL] atomically:YES];
                     
                     NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil];
                     [NSURLCache setSharedURLCache:sharedCache];
                     
                     dispatch_sync(dispatch_get_main_queue(), ^{
                         [UIView transitionWithView:self
                                           duration:kDuration
                                            options: animationType|UIViewAnimationOptionAllowUserInteraction
                                         animations:^{
                                             self.image = image;
                                         }
                                         completion:NULL];
                         [self setModeForImage:_contentModeType];
                     });
                 });
             }
             else if (([data length] == 0 || !image) && error == nil ){
                 //empty reply
                              NSLog(@"empty data");
             }
             else if (error != nil && error.code == kTimeout){//ERROR_CODE_TIMEOUT
                 //time out
                              NSLog(@"request time out");
             }
             else if (error != nil){
                 //error
                              NSLog(@"error occured");
             }
         }
     }];
}

#pragma mark
#pragma mark - REQUEST FOR IMAGE URL
-(void)requestForCropImage:(NSString*)strURL{
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    NSURLRequest *request;
    NSString * newImageURL = [strURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    request = [NSURLRequest requestWithURL:[NSURL URLWithString:newImageURL]];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         UIImage *image = [UIImage imageWithData:data];
         
         if ([data length] > 0 && error == nil && image)  //If  success
         {
             [[FullyLoaded sharedFullyLoaded] setImage:image withKey:strURL];
             if ([strURL isEqualToString:strCurrUrl])
             {
                 //recive data
                 dispatch_queue_t queue_ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                 dispatch_async(queue_, ^{
                     
                     UIImage *image = [[FullyLoaded sharedFullyLoaded] imageForURL:strURL];
                     
                     [self performSelectorOnMainThread:@selector(receivedImage:) withObject:image waitUntilDone:NO];
                     
                     [data writeToFile:[[NSUserDefaults standardUserDefaults] valueForKey:strURL] atomically:YES];
                 });
             }
             else if (([data length] == 0 || !image) && error == nil ){
                 //empty reply
             }
             else if (error != nil && error.code == kTimeout){//ERROR_CODE_TIMEOUT
                 //time out
             }
             else if (error != nil){
                 //error
             }
         }
     }];
}
#pragma mark
#pragma mark - Method will call from CropImage
-(void)setImageWithURLforCrop:(NSString*)strURL placeholderImage:(UIImage*)placeholdImage
{
    strCurrUrl =  strURL;       //Store current Url
    
    UIImage *image = [[FullyLoaded sharedFullyLoaded] imageForURL:strURL];
    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        @autoreleasepool {
            if (image)
            {
                self.image = image;
                [self performSelectorOnMainThread:@selector(receivedImage:) withObject:image waitUntilDone:NO];
                [self setModeForImage:_contentModeType];
            }
            else
            {
                if (placeholdImage) {
                    self.image = placeholdImage;//Set placeholder image to self(UIImageView)
                    
                    if (placeholdImage.size.width > self.frame.size.width || placeholdImage.size.height > self.frame.size.height) {
                        
                        [self setModeForImage:UIViewContentModeScaleAspectFill];
                    }
                    else
                    {
                        [self setModeForImage:UIViewContentModeCenter];
                    }
                }
                
                [self requestForCropImage:strURL];
            }
            
        }
        
    });
    
}
#pragma mark
#pragma mark - TOUCH DELEGATE METHOD
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    if (touch.tapCount == 2) {
        //This will cancel the singleTap action
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
    }
    
    if ([touch view] == self)
    {
        if ([_asyncImageDelegate respondsToSelector:@selector(asyncImageView:didSelectWithURL:)]) {
            
            [_asyncImageDelegate asyncImageView:self didSelectWithURL:strCurrUrl];
        }
    }
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSUInteger numTaps = [[touches anyObject] tapCount];
    float delay = 0.2;

        if (numTaps < 2)
        {
            [self performSelector:@selector(handleSingleTap) withObject:nil afterDelay:delay ];
            [self.nextResponder touchesEnded:touches withEvent:event];
        }
        else if(numTaps == 2)
        {
            [NSObject cancelPreviousPerformRequestsWithTarget:self];
            [self performSelector:@selector(handleDoubleTap) withObject:nil afterDelay:delay ];
        }
    
}
-(void)handleSingleTap{
    if ([_asyncImageDelegate respondsToSelector:@selector(asyncImageView:didSelectEndWithURL:withTapCount:)]) {
        
        [_asyncImageDelegate asyncImageView:self didSelectEndWithURL:strCurrUrl withTapCount:1];
    }
}

-(void)handleDoubleTap{
    if ([_asyncImageDelegate respondsToSelector:@selector(asyncImageView:didSelectEndWithURL:withTapCount:)]) {
        
        [_asyncImageDelegate asyncImageView:self didSelectEndWithURL:strCurrUrl withTapCount:2];
    }
}

-(void)receivedImage:(UIImage *)image {
    
    _isDownloaded = YES;
    
    if ([_asyncImageDelegate respondsToSelector:@selector(asyncImageView:didReceiveImage:withURL:)])
    {
        [self stopAnimateIndicator];
        [_asyncImageDelegate asyncImageView:self didReceiveImage:image withURL:strCurrUrl];
    }
}



-(void)dealloc
{
    if ([timerAnim isValid])
    {
        [timerAnim invalidate];
        timerAnim = nil;
    }
    id __weak localdelegate = self.asyncImageDelegate;
    self.asyncImageDelegate = nil;
    localdelegate  = nil;
}
@end
