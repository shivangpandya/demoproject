

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FullyLoaded : NSObject
{
}

+ (FullyLoaded *)sharedFullyLoaded;

- (void) emptyCache;
-(void)removeCachedImageForURL :(NSString*)imageURL;
- (void) removeAllCacheDownloads;
- (UIImage*) imageForURL:(NSString*)imageURL;
- (NSString*) pathForImageURL:(NSString*)imageURL;

// path related
+ (NSString*) filePathForResourceAtURL:(NSString*)url;
+ (BOOL)      fileExistsForResourceAtURL:(NSString*)url;

+ (NSString*) tmpFilePathForResourceAtURL:(NSString*)url;
+ (BOOL)      tmpFileExistsForResourceAtURL:(NSString*)url;

-(void)setImage:(UIImage*)image withKey:(NSString*)strKey;

@end
