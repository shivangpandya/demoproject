//
//  ViewController.m
//  Gokul Groups
//
//  Created by Shivang on 07/06/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import "ViewController.h"
#import "MainVC.h"

#import "Constants.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "UserDefaultHelper.h"
#import "MBProgressHUD.h"
@interface ViewController (){
    
    MBProgressHUD *HUD;

}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.navigationItem setHidesBackButton:YES];
    self.navigationController.navigationBarHidden = YES;
    [self setNeedsStatusBarAppearanceUpdate];

   
    scrlview.contentSize = CGSizeMake(0, 500);

     [onswitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    
    onswitch.on=YES;
    hidevw.hidden=NO;

    skipbtn.layer.borderColor=[UIColor redColor].CGColor;
    skipbtn.layer.borderWidth=2.0f;
    skipbtn.layer.cornerRadius=3.0f;
    
    regbtn.layer.borderColor=[UIColor redColor].CGColor;
    regbtn.layer.borderWidth=2.0f;
    regbtn.layer.cornerRadius=3.0f;
    
    tfname.text= @"";
    tfemail.text= @"";
    tfphone.text= @"";
    tfreg.text= @"";

    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)changeSwitch:(id)sender{
    if([sender isOn]){
        NSLog(@"Switch is ON");
        hidevw.hidden=NO;
        downvw.frame=CGRectMake(8, hidevw.frame.origin.y+hidevw.frame.size.height+8, downvw.frame.size.width, downvw.frame.size.height);
    } else{
        NSLog(@"Switch is OFF");
        hidevw.hidden=YES;
        downvw.frame=CGRectMake(8, hidevw.frame.origin.y+hidevw.frame.size.height-50, downvw.frame.size.width, downvw.frame.size.height);

    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)viewDidLayoutSubviews{
   
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    downvw.frame=CGRectMake(8, hidevw.frame.origin.y+hidevw.frame.size.height+8, downvw.frame.size.width, downvw.frame.size.height);
    
}

-(IBAction)btnskipclick:(id)sender{
    MainVC *controller = [[MainVC alloc]initWithNibName:@"MainVC" bundle:nil];
    [self.navigationController pushViewController:controller animated:NO];
    
    
}
-(IBAction)btnregisterclick:(id)sender{
    
    if ([tfname.text isEqualToString:@""]||[tfphone.text isEqualToString:@""]||[tfemail.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Please enter all details."
                                                       delegate:self
                                              cancelButtonTitle:@"ok"
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else {
        
        NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
        [dict setValue:tfname.text forKey:@"name"];
        [dict setValue:tfphone.text forKey:@"phone"];
        [dict setValue:tfemail.text forKey:@"email"];
        [dict setValue:tfreg.text forKey:@"reg"];
        [[UserDefaultHelper sharedObject] setUserInfo:dict.mutableCopy];
        
        MainVC *controller = [[MainVC alloc]initWithNibName:@"MainVC" bundle:nil];
        [self.navigationController pushViewController:controller animated:NO];


    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
