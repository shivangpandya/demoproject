//
//  MainVC.h
//  Gokul Groups
//
//  Created by Shivang on 08/06/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainVC : UIViewController {
    
    IBOutlet UIView *bikevw;
    IBOutlet UIView *carvw;
    IBOutlet UIView *virajvw;
    IBOutlet UIView *sclvw;
    
}

-(IBAction)bikebtnclick:(id)sender;
-(IBAction)carbtnclick:(id)sender;
-(IBAction)virajbtnclick:(id)sender;
-(IBAction)schoolbtnclick:(id)sender;

@end
