//
//  AllCarVC.h
//  Gokul Groups
//
//  Created by Shivang on 19/07/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "UserDefaultHelper.h"
#import "MBProgressHUD.h"

@interface AllCarVC : UIViewController{
    
    MBProgressHUD *HUD;
    
    IBOutlet UICollectionView *CategoryCollectionView;
    
    NSMutableArray *arrResponse;
    
}
-(IBAction)backbtnclick:(id)sender;

@end
