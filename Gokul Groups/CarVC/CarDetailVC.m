//
//  CarDetailVC.m
//  Gokul Groups
//
//  Created by Shivang on 16/07/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import "CarDetailVC.h"
#import "DetailCell.h"
#import "Constants.h"
#import "FormVC.h"
#import "ColorCell.h"

@interface CarDetailVC (){
    IBOutlet UIButton *cancelbtn;
    IBOutlet UIButton *sendbtn;
    IBOutlet UIView *bottomvw;
    
}


@end

@implementation CarDetailVC
@synthesize arrdetail;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    sendbtn.layer.borderWidth=2.0f;
    sendbtn.layer.borderColor=[UIColor redColor].CGColor;
    sendbtn.layer.cornerRadius=3.0f;
    cancelbtn.layer.borderWidth=2.0f;
    cancelbtn.layer.borderColor=[UIColor redColor].CGColor;
    cancelbtn.layer.cornerRadius=3.0f;
    

    if ([[arrdetail valueForKey:@"imageUrl"]isKindOfClass:[NSString class]])  {
        NSLog(@"Image is %@",[arrdetail valueForKey:@"imageUrl"] );
        [pic setImageWithURL:[arrdetail valueForKey:@"imageUrl"]placeholderImage:[UIImage imageNamed:@"car_ic"]];
        
    }
    else{
        pic.image=[UIImage imageNamed:@"car_ic"];
    }
    
    lblname.text = [arrdetail valueForKey:@"carName"];
    
    [tblviewC registerNib:[UINib nibWithNibName:@"ColorCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    NSMutableArray * arr = [arrdetail valueForKey:@"colorImageUrl"];
    colorary = [[NSMutableArray alloc] init];
    colorary1 = [[NSMutableArray alloc]init];
    for (int i=0; i<arr.count; i++) {
        NSString * str = [[arr valueForKey:@"Description"] objectAtIndex:i];
        [colorary addObject:str];
        NSString * str1 = [[arr valueForKey:@"ImagePath"] objectAtIndex:i];
        [colorary1 addObject:str1];
        
    }

    [tblviewD registerNib:[UINib nibWithNibName:@"DetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    dimary =[[NSMutableArray alloc] initWithObjects:@"Overall Length(mm)",@"Overall Width(mm)",@"Overall Height(mm)",@"Wheelbase(mm)",@"Front Track(mm)",@"Rear Track(mm)",@"Ground Clearance",@"Turning Radius(m)",@"Boot Space(l)",@"Fuel Tank Capacity", nil];
    dimary1 =[[NSMutableArray alloc] initWithObjects:[arrdetail valueForKey:@"dimLength"],[arrdetail valueForKey:@"dimWidth"],[arrdetail valueForKey:@"dimHeight"],[arrdetail valueForKey:@"dimWheelbase"],[arrdetail valueForKey:@"dimFrontTrack"],[arrdetail valueForKey:@"dimRearTrack"],[arrdetail valueForKey:@"dimGroundClearance"],[arrdetail valueForKey:@"dimTurningRadius"],[arrdetail valueForKey:@"dimBootSpace"],[arrdetail valueForKey:@"dimFuelCapacity"], nil];

   
    
    [tblviewE registerNib:[UINib nibWithNibName:@"DetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    engineary =[[NSMutableArray alloc] initWithObjects:@"Displacement",@"Compression Ratio",@"Transmission",@"NumberOf Gears",@"Engine Type",@"Max Engine Output",@"Maximum Torque",@"Maximum Speed", nil];
    engineary1 =[[NSMutableArray alloc] initWithObjects:[arrdetail valueForKey:@"esDisplacement"],[arrdetail valueForKey:@"esCompressionRatio"],[arrdetail valueForKey:@"esTransmission"],[arrdetail valueForKey:@"esNumberGears"],[arrdetail valueForKey:@"esEngineType"],[arrdetail valueForKey:@"esMaxEngineOutput"],[arrdetail valueForKey:@"esMaxTorque"],[arrdetail valueForKey:@"esMaxSpeed"], nil];
    
        
    [tblviewS registerNib:[UINib nibWithNibName:@"DetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    susary =[[NSMutableArray alloc] initWithObjects:@"Front",@"Rear", nil];
    susary1 =[[NSMutableArray alloc] initWithObjects:[arrdetail valueForKey:@"suspensionFront"],[arrdetail valueForKey:@"suspensionRear"], nil];
    
    
    
    [tblviewB registerNib:[UINib nibWithNibName:@"DetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    brakeary =[[NSMutableArray alloc] initWithObjects:@"Brakes Front",@"Brakes Rear", nil];
    brakeary1 =[[NSMutableArray alloc] initWithObjects:[arrdetail valueForKey:@"breakFront"],[arrdetail valueForKey:@"breakRear"], nil];
    
    
    
    [tblviewStr registerNib:[UINib nibWithNibName:@"DetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    strary =[[NSMutableArray alloc] initWithObjects:@"Type", nil];
    strary1 =[[NSMutableArray alloc] initWithObjects:[arrdetail valueForKey:@"steeringType"], nil];
    
       
    [tblviewW registerNib:[UINib nibWithNibName:@"DetailCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    wheelary =[[NSMutableArray alloc] initWithObjects:@"Wheels",@"Tyres", nil];
    wheelary1 =[[NSMutableArray alloc] initWithObjects:[arrdetail valueForKey:@"wheels"],[arrdetail valueForKey:@"tyre"], nil];
    
    
    
    
   
    
    
//    if (IS_IPHONE_6 || IS_IPHONE_6S)
//    {space=230;}
//    else if (IS_IPHONE_6_PLUS || IS_IPHONE_6S_PLUS)
//    {space=270;}
//    else
//    {space=180;}
    space = 165;

    
}
-(void)viewDidLayoutSubviews{
    
    [tblviewC reloadData];
    [tblviewD reloadData];
    [tblviewE reloadData];
    [tblviewS reloadData];
    [tblviewB reloadData];
    [tblviewStr reloadData];
    [tblviewW reloadData];
    
    tblviewC.frame = CGRectMake(5, 135, [UIScreen mainScreen].bounds.size.width-10, tblviewC.contentSize.height);
    tblviewC.layer.borderColor=[UIColor grayColor].CGColor;
    tblviewC.layer.borderWidth=2.0f;
    tblviewC.tableHeaderView=hviewc;
    [scrlvw addSubview:tblviewC];
    
    tblviewD.frame = CGRectMake(5, tblviewC.frame.origin.y+tblviewC.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, tblviewD.contentSize.height);
    tblviewD.layer.borderColor=[UIColor grayColor].CGColor;
    tblviewD.layer.borderWidth=2.0f;
    tblviewD.tableHeaderView=hview;
    [scrlvw addSubview:tblviewD];
    
    tblviewE.frame = CGRectMake(5, tblviewD.frame.origin.y+tblviewD.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, tblviewE.contentSize.height);
    tblviewE.layer.borderColor=[UIColor grayColor].CGColor;
    tblviewE.layer.borderWidth=2.0f;
    tblviewE.tableHeaderView=hview1;
    [scrlvw addSubview:tblviewE];
    
    tblviewS.frame = CGRectMake(5, tblviewE.frame.origin.y+tblviewE.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, tblviewS.contentSize.height);
    tblviewS.layer.borderColor=[UIColor grayColor].CGColor;
    tblviewS.layer.borderWidth=2.0f;
    tblviewS.tableHeaderView=hview2;
    [scrlvw addSubview:tblviewS];
    
    tblviewB.frame = CGRectMake(5, tblviewS.frame.origin.y+tblviewS.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, tblviewB.contentSize.height);
    tblviewB.layer.borderColor=[UIColor grayColor].CGColor;
    tblviewB.layer.borderWidth=2.0f;
    tblviewB.tableHeaderView=hview3;
    [scrlvw addSubview:tblviewB];
    
    tblviewStr.frame = CGRectMake(5, tblviewB.frame.origin.y+tblviewB.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, tblviewStr.contentSize.height);
    tblviewStr.layer.borderColor=[UIColor grayColor].CGColor;
    tblviewStr.layer.borderWidth=2.0f;
    tblviewStr.tableHeaderView=hview4;
    [scrlvw addSubview:tblviewStr];
    
    tblviewW.frame = CGRectMake(5, tblviewStr.frame.origin.y+tblviewStr.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, tblviewW.contentSize.height);
    tblviewW.layer.borderColor=[UIColor grayColor].CGColor;
    tblviewW.layer.borderWidth=2.0f;
    tblviewW.tableHeaderView=hview5;
    [scrlvw addSubview:tblviewW];
    
    
    bottomvw.frame = CGRectMake(5, tblviewW.frame.origin.y+tblviewW.frame.size.height+10, [UIScreen mainScreen].bounds.size.width-10, bottomvw.frame.size.height);
    [scrlvw addSubview:bottomvw];
    scrlvw.contentSize = CGSizeMake(0, bottomvw.frame.origin.y+bottomvw.frame.size.height+10);
    
}
-(IBAction)backbtnclick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==tblviewC) {
        return colorary.count;
    }
    
    if (tableView==tblviewD) {
        return dimary.count;
    }
    if (tableView==tblviewE) {
        return engineary.count;
        
    }
    if (tableView==tblviewS) {
        return susary.count;
        
    }
    if (tableView==tblviewB) {
        return brakeary.count;
        
    }
    if (tableView==tblviewStr) {
        return strary.count;
        
    }
    else  {
        return wheelary.count;
        
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tblviewC) {
        return 60;
    }
    else{
    int topPadding = 13;
    int bottomPadding = 13;
    
    NSString *text;
    
    if (tableView==tblviewD) {
        text = [dimary1 objectAtIndex:[indexPath row]];
    }
    
    
    if (tableView==tblviewE) {
        text = [engineary1 objectAtIndex:[indexPath row]];
    }
    if (tableView==tblviewS) {
        text = [susary1 objectAtIndex:[indexPath row]];
        
    }
    if (tableView==tblviewB) {
        text = [brakeary1 objectAtIndex:[indexPath row]];
        
    }
    if (tableView==tblviewStr) {
        text = [strary1 objectAtIndex:[indexPath row]];
        
    }
    if (tableView==tblviewW) {
        text = [wheelary1 objectAtIndex:[indexPath row]];
        
    }
    
    
    CGRect textRect = [text boundingRectWithSize:
                       CGSizeMake([UIScreen mainScreen].bounds.size.width-space,CGFLOAT_MAX)
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName:REGULAR_FONT(12)}
                                         context:nil];
    if (textRect.size.height+26 > 44) {
        return topPadding+textRect.size.height+bottomPadding;
    }
    else{
        return 44;
    }
}
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==tblviewC) {
        
        static NSString *CellIdentifier = @"cell";
        
        ColorCell* cell =  (ColorCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.lblname.text =[colorary objectAtIndex:indexPath.row];
        
        [cell.img setImageWithURL:[colorary1 objectAtIndex:indexPath.row] placeholderImage:[UIImage imageNamed:@"car_ic"]];
        
        return cell;
    }
    
    else{
    
    static NSString *CellIdentifier = @"cell";
    
    DetailCell* cell =  (DetailCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (tableView==tblviewD) {
        cell.lblname.text =[dimary objectAtIndex:indexPath.row];
        
        CGRect textRect = [[dimary1 objectAtIndex:[indexPath row]] boundingRectWithSize:
                           CGSizeMake([UIScreen mainScreen].bounds.size.width-space,CGFLOAT_MAX)
                                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                                attributes:@{NSFontAttributeName:REGULAR_FONT(12)}
                                                                                   context:nil];
        
        cell.lbldisc.frame=CGRectMake(150, 8, [UIScreen mainScreen].bounds.size.width-space, textRect.size.height);
        
        cell.lbldisc.text =[dimary1 objectAtIndex:indexPath.row];
    }
    if (tableView==tblviewE) {
        
        cell.lblname.text =[engineary objectAtIndex:indexPath.row];
        
        CGRect textRect = [[engineary1 objectAtIndex:[indexPath row]] boundingRectWithSize:
                           CGSizeMake([UIScreen mainScreen].bounds.size.width-space,CGFLOAT_MAX)
                                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                                attributes:@{NSFontAttributeName:REGULAR_FONT(12)}
                                                                                   context:nil];
        
        cell.lbldisc.frame=CGRectMake(150, 8, [UIScreen mainScreen].bounds.size.width-space, textRect.size.height);
        
        cell.lbldisc.text =[engineary1 objectAtIndex:indexPath.row];
    }
    if (tableView==tblviewS) {
        cell.lblname.text =[susary objectAtIndex:indexPath.row];
        CGRect textRect = [[susary1 objectAtIndex:[indexPath row]] boundingRectWithSize:
                           CGSizeMake([UIScreen mainScreen].bounds.size.width-space,CGFLOAT_MAX)
                                                                               options:NSStringDrawingUsesLineFragmentOrigin
                                                                            attributes:@{NSFontAttributeName:REGULAR_FONT(12)}
                                                                               context:nil];
        
        cell.lbldisc.frame=CGRectMake(150, 8, [UIScreen mainScreen].bounds.size.width-space, textRect.size.height);
        
        cell.lbldisc.text =[susary1 objectAtIndex:indexPath.row];
    }
    if (tableView==tblviewB) {
        cell.lblname.text =[brakeary objectAtIndex:indexPath.row];
        CGRect textRect = [[brakeary1 objectAtIndex:[indexPath row]] boundingRectWithSize:
                           CGSizeMake([UIScreen mainScreen].bounds.size.width-space,CGFLOAT_MAX)
                                                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                                                               attributes:@{NSFontAttributeName:REGULAR_FONT(12)}
                                                                                  context:nil];
        
        cell.lbldisc.frame=CGRectMake(150, 8, [UIScreen mainScreen].bounds.size.width-space, textRect.size.height);
        
        cell.lbldisc.text =[brakeary1 objectAtIndex:indexPath.row];
    }
    if (tableView==tblviewStr) {
        cell.lblname.text =[strary objectAtIndex:indexPath.row];
        CGRect textRect = [[strary1 objectAtIndex:[indexPath row]] boundingRectWithSize:
                           CGSizeMake([UIScreen mainScreen].bounds.size.width-space,CGFLOAT_MAX)
                                                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                                                               attributes:@{NSFontAttributeName:REGULAR_FONT(12)}
                                                                                  context:nil];
        
        cell.lbldisc.frame=CGRectMake(150, 8, [UIScreen mainScreen].bounds.size.width-space, textRect.size.height);
        
        cell.lbldisc.text =[strary1 objectAtIndex:indexPath.row];
    }
    if (tableView==tblviewW) {
        cell.lblname.text =[wheelary objectAtIndex:indexPath.row];
        CGRect textRect = [[wheelary1 objectAtIndex:[indexPath row]] boundingRectWithSize:
                           CGSizeMake([UIScreen mainScreen].bounds.size.width-space,CGFLOAT_MAX)
                                                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                                                              attributes:@{NSFontAttributeName:REGULAR_FONT(12)}
                                                                                 context:nil];
        
        cell.lbldisc.frame=CGRectMake(150, 8, [UIScreen mainScreen].bounds.size.width-space, textRect.size.height);
        
        cell.lbldisc.text =[wheelary1 objectAtIndex:indexPath.row];
    }
    
    
       return cell;
        
    }
    
}

-(IBAction)btncancelclick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)btnsendclick:(id)sender{
    
    FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
    controller.isfrom=@"cinquiry";
    controller.detailis =[NSString stringWithFormat:@"Inquiry for %@",[arrdetail valueForKey:@"carName"]] ;
    [self.navigationController pushViewController:controller animated:YES];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
