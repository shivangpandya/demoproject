//
//  CarOpenVC.m
//  Gokul Groups
//
//  Created by Shivang on 15/07/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import "CarOpenVC.h"
#import "OfferCell.h"
#import "AllCarVC.h"
#import "FormVC.h"
#import "ViewController.h"
#import "AboutUsVC1.h"
#import "ContactUsVC1.h"

@interface CarOpenVC ()

@end

@implementation CarOpenVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNeedsStatusBarAppearanceUpdate];
    
    [OfferCollectionView registerNib:[UINib nibWithNibName:@"OfferCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    OfferCollectionView.backgroundColor = [UIColor clearColor];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(IBAction)backbtnclick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - UICollectionView Delegate Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 10;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OfferCell *cell = (OfferCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    if (indexPath.row==0) {
        cell.bgvw.image=[UIImage imageNamed:@"c1"];
    }
    if (indexPath.row==1) {
        cell.bgvw.image=[UIImage imageNamed:@"c2"];
    }if (indexPath.row==2) {
        cell.bgvw.image=[UIImage imageNamed:@"c3"];
    }if (indexPath.row==3) {
        cell.bgvw.image=[UIImage imageNamed:@"c4"];
    }if (indexPath.row==4) {
        cell.bgvw.image=[UIImage imageNamed:@"c5"];
    }if (indexPath.row==5) {
        cell.bgvw.image=[UIImage imageNamed:@"c6"];
    }if (indexPath.row==6) {
        cell.bgvw.image=[UIImage imageNamed:@"c7"];
    }if (indexPath.row==7) {
        cell.bgvw.image=[UIImage imageNamed:@"c8"];
    }if (indexPath.row==8) {
        cell.bgvw.image=[UIImage imageNamed:@"c9"];
    }if (indexPath.row==9) {
        cell.bgvw.image=[UIImage imageNamed:@"c10"];
    }
    return cell;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE_6 || IS_IPHONE_6S)
    {
        return CGSizeMake(186, 186);
    }
    else if (IS_IPHONE_6_PLUS || IS_IPHONE_6S_PLUS)
    {
        return CGSizeMake(206, 206);
    }
    else
    {
        return CGSizeMake(158 , 158);
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        AllCarVC *controller = [[AllCarVC alloc]initWithNibName:@"AllCarVC" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (indexPath.row==1) {
        FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
        controller.isfrom=@"cinquiry";
        controller.detailis=@"";
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    
    else if (indexPath.row==2) {
        FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
        controller.isfrom=@"cservice";
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else if (indexPath.row==3) {
        FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
        controller.isfrom=@"cparts";
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (indexPath.row==4) {
        FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
        controller.isfrom=@"cexchange";
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (indexPath.row==5) {
        FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
        controller.isfrom=@"cinsurance";
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else if (indexPath.row==6) {
        AboutUsVC1 *controller = [[AboutUsVC1 alloc]initWithNibName:@"AboutUsVC1" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else if (indexPath.row==7) {
        FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
        controller.isfrom=@"ccareer";
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else if (indexPath.row==8) {
        ContactUsVC1 *controller = [[ContactUsVC1 alloc]initWithNibName:@"ContactUsVC1" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    else if (indexPath.row==9) {
        FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
        controller.isfrom=@"cfeedback";
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
