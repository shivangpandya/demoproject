//
//  CarDetailVC.h
//  Gokul Groups
//
//  Created by Shivang on 16/07/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImage.h"

@interface CarDetailVC : UIViewController{
    
    IBOutlet UITableView *tblviewC;
    NSMutableArray * colorary;
    NSMutableArray * colorary1;
    IBOutlet UIView *hviewc;
    
    IBOutlet UITableView *tblviewD;
    NSMutableArray * dimary;
    NSMutableArray * dimary1;
    IBOutlet UIView *hview;
    
    IBOutlet UITableView *tblviewE;
    NSMutableArray * engineary;
    NSMutableArray * engineary1;
    IBOutlet UIView *hview1;
    
    IBOutlet UITableView *tblviewS;
    NSMutableArray * susary;
    NSMutableArray * susary1;
    IBOutlet UIView *hview2;

    IBOutlet UITableView *tblviewB;
    NSMutableArray * brakeary;
    NSMutableArray * brakeary1;
    IBOutlet UIView *hview3;
    
    IBOutlet UITableView *tblviewStr;
    NSMutableArray * strary;
    NSMutableArray * strary1;
    IBOutlet UIView *hview4;
    
    IBOutlet UITableView *tblviewW;
    NSMutableArray * wheelary;
    NSMutableArray * wheelary1;
    IBOutlet UIView *hview5;
    
    IBOutlet UIScrollView *scrlvw;
    
    int space;
    
    IBOutlet AsyncImage *pic;
    
    IBOutlet UILabel *lblname;

    
}
-(IBAction)backbtnclick:(id)sender;

@property(retain,nonatomic)NSMutableArray * arrdetail;

@end
