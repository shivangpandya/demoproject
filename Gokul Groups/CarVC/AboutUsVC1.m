//
//  AboutUsVC1.m
//  Gokul Groups
//
//  Created by Shivang on 28/09/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import "AboutUsVC1.h"

@interface AboutUsVC1 (){
    IBOutlet UITextView *tfmsg;
    
}


@end

@implementation AboutUsVC1

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNeedsStatusBarAppearanceUpdate];
    
    tfmsg.editable = NO;
    
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    [tfmsg scrollRectToVisible:CGRectMake(0,0,1,1) animated:YES];
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    NSString * str = @"<h2>Early Beginnings</h2>Mr. Milan M. Pobaru is one of the leading and prestigious businessmen in Jamnagar, Gujarat with his prevalent business roots in Automobile dealership, construction and education sector.<br /><br /> In a short span of time he has transformed all his business ventures to a top notch position, especially in automobile dealership sector.<br /><br />Mr. Pobaru had started his business carrier since 1975. Along with his studies he started the business of Petroleum products i.e. LDO, SKO lubricating oil etc. where he had record breaking sales. He had also made remarkable sales in Gujarat of MTO in the year 1991&#8211;92.<br /><br /> <h2>Gokul Ford</h2>Mr.Pobaru is the authorized dealer of Ford which is world’s no.1 manufacturer of four wheelers. Mr. Pobaru  acquired the dealership of Ford cars in Jamnagar in the year of 2015, and Mr. Pobaru was instrumental in defining this space that is now defining many aspects of the automobile dealership business sector.<br /><br />With seamless dedication in sales strategies and customer satisfaction he is able to create an enviable position in the Jamnagar.<br /><br />www.gokulgroups.com";
    
    str = [str stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-size:%fpx;}</style>",16.0]];
    
    tfmsg.attributedText = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUTF8StringEncoding]options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}documentAttributes:nil error:nil];
    

    
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(IBAction)btnbackclick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
