//
//  AdmissionVC.h
//  Gokul Groups
//
//  Created by Shivang on 26/08/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMPickerView.h"
#import "Constants.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
#import "UserDefaultHelper.h"
#import "MBProgressHUD.h"

@interface AdmissionVC : UIViewController {
    
    IBOutlet UIScrollView *scrlview;
    IBOutlet UITextField *tfname;
    IBOutlet UITextField *tfstd;
    IBOutlet UITextField *tfschool;
    IBOutlet UITextField *tfcity;
    IBOutlet UITextField *tfmobile;
    IBOutlet UITextField *tfphone;
    IBOutlet UITextField *tfoption;
    IBOutlet UITextField *tfemail;
    IBOutlet UIButton*cancelbtn;
    IBOutlet UIButton *sendbtn;
    
    IBOutlet UIButton *btn1;
    IBOutlet UIButton *btn2;
    IBOutlet UIButton *btn3;
    IBOutlet UIButton *btn4;
    
    MBProgressHUD *HUD;


}

-(IBAction)btnbackclick:(id)sender;
-(IBAction)btnoptionclick:(id)sender;

@end
