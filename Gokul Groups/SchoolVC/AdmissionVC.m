//
//  AdmissionVC.m
//  Gokul Groups
//
//  Created by Shivang on 26/08/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import "AdmissionVC.h"

@interface AdmissionVC (){
    
    NSString*medstr;
    
}

@end

@implementation AdmissionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNeedsStatusBarAppearanceUpdate];
    
    sendbtn.layer.borderWidth=2.0f;
    sendbtn.layer.borderColor=[UIColor redColor].CGColor;
    sendbtn.layer.cornerRadius=3.0f;
    cancelbtn.layer.borderWidth=2.0f;
    cancelbtn.layer.borderColor=[UIColor redColor].CGColor;
    cancelbtn.layer.cornerRadius=3.0f;
    
    scrlview.contentSize = CGSizeMake(0 , sendbtn.frame.origin.y+60);
    
    [btn1 sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    if ([[UserDefaultHelper sharedObject] userInfo]) {
        tfname.text = [[[UserDefaultHelper sharedObject] userInfo] valueForKey:@"name"];
        tfmobile.text = [[[UserDefaultHelper sharedObject] userInfo] valueForKey:@"phone"];
        tfemail.text = [[[UserDefaultHelper sharedObject] userInfo] valueForKey:@"email"];
    }

}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(IBAction)btnbackclick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction) buttonPressed1:(id)sender {
    [btn1 setSelected:NO];
    [btn2 setSelected:NO];
    [btn3 setSelected:NO];
    [btn4 setSelected:NO];

    if ([sender isSelected]) {
        [sender setSelected: NO];
    } else {
        [sender setSelected: YES];
    }
    medstr = @"CBSE";
    
}

- (IBAction) buttonPressed2:(id)sender {
    [btn1 setSelected:NO];
    [btn2 setSelected:NO];
    [btn3 setSelected:NO];
    [btn4 setSelected:NO];
    if ([sender isSelected]) {
        [sender setSelected: NO];
    } else {
        [sender setSelected: YES];
    }
    
    medstr = @"State Board";

    
}
- (IBAction) buttonPressed3:(id)sender {
    [btn1 setSelected:NO];
    [btn2 setSelected:NO];
    [btn3 setSelected:NO];
    [btn4 setSelected:NO];
    if ([sender isSelected]) {
        [sender setSelected: NO];
    } else {
        [sender setSelected: YES];
    }
    
    medstr = @"ICSE";

    
}
- (IBAction) buttonPressed4:(id)sender {
    [btn1 setSelected:NO];
    [btn2 setSelected:NO];
    [btn3 setSelected:NO];
    [btn4 setSelected:NO];
    if ([sender isSelected]) {
        [sender setSelected: NO];
    } else {
        [sender setSelected: YES];
    }
    medstr = @"Others";

}

-(IBAction)btnoptionclick:(id)sender{
    
    [self.view endEditing:YES];
    NSMutableArray *arr=[[NSMutableArray alloc ]initWithObjects:@"Existing student",@"Website",@"News Paper",@"Hoarding",@"Mouth Publicity",@"Cinema Slide",@"Leaflet",@"Others", nil];
    [MMPickerView showPickerViewInView:self.view
                           withStrings:arr
                           withOptions:nil
                            completion:^(NSString *selectedString) {
                                tfoption.text = selectedString;
                            }];
}

-(IBAction)btnsendclick:(id)sender{
    
    if ([tfname.text isEqualToString:@""]||[tfstd.text isEqualToString:@""]||[tfschool.text isEqualToString:@""]||[tfcity.text isEqualToString:@""]||[tfemail.text isEqualToString:@""]||[tfmobile.text isEqualToString:@""]||[tfoption.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Please enter all details."
                                                       delegate:self
                                              cancelButtonTitle:@"ok"
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else{
        
        
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.mode = MBProgressHUDModeCustomView;
        HUD.customView = [ApplicationDelegate explosion];
        HUD.color = [UIColor clearColor];
        HUD.dimBackground = YES;
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        [manager setResponseSerializer:responseSerializer];
        
        
        NSString * api =  [NSString stringWithFormat:@"http://gokulgroups.com/Services/api/InquiryModels"];
        
        NSString * msg =  [NSString stringWithFormat:@"Seeking Admission in std %@ \n Previous school attended %@ \n Previous school affiliated to %@ \n City %@ \n Parent's Phone No %@ \n Got reference of the school from %@ \n",tfstd.text,tfschool.text,medstr,tfcity.text,tfmobile.text,tfoption.text];
        NSDictionary *parameters = @{@"DBID":@0,@"group":@"SUNFLOWER",@"sub_group":@"ADMISSION_FORM",@"name":tfname.text,@"phone":tfmobile.text,@"email":tfemail.text,@"message":msg};
        
        [manager POST:api parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             [HUD hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                             message:@"Your inquiry has been submitted successfully. Our representative will contact you back on your provided number.Thank you."
                                                            delegate:self
                                                   cancelButtonTitle:@"ok"
                                                   otherButtonTitles:nil, nil];
             [alert show];
             [self.navigationController popViewControllerAnimated:YES];
             
             
             
         }
              failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                             message:@"Server down or issue with the internet connection. Please check your connection."
                                                            delegate:self
                                                   cancelButtonTitle:@"ok"
                                                   otherButtonTitles:nil, nil];
             [alert show];
             
             NSLog(@"Error: %@", error);
             [HUD hide:YES];
             
         }];
        
        
        
    }
}


-(IBAction)btncancelclick:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
