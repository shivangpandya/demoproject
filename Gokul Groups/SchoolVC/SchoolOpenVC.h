//
//  SchoolOpenVC.h
//  Gokul Groups
//
//  Created by Shivang on 26/08/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OfferCell.h"
#import "ViewController.h"

@interface SchoolOpenVC : UIViewController{
    
    IBOutlet UICollectionView* OfferCollectionView;
    
}

-(IBAction)backbtnclick:(id)sender;

@end
