//
//  SchoolOpenVC.m
//  Gokul Groups
//
//  Created by Shivang on 26/08/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import "SchoolOpenVC.h"
#import "FormVC.h"
#import "AdmissionVC.h"
#import "ManagementVC.h"
#import "AboutUsVC2.h"
#import "VisionVC.h"
#import "ContactUsVC3.h"

@interface SchoolOpenVC ()

@end

@implementation SchoolOpenVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    [OfferCollectionView registerNib:[UINib nibWithNibName:@"OfferCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    OfferCollectionView.backgroundColor = [UIColor clearColor];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(IBAction)backbtnclick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - UICollectionView Delegate Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 6;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OfferCell *cell = (OfferCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    if (indexPath.row==0) {
        cell.bgvw.image=[UIImage imageNamed:@"s1"];
    }
    if (indexPath.row==1) {
        cell.bgvw.image=[UIImage imageNamed:@"s2"];
    }if (indexPath.row==2) {
        cell.bgvw.image=[UIImage imageNamed:@"s3"];
    }if (indexPath.row==3) {
        cell.bgvw.image=[UIImage imageNamed:@"s4"];
    }if (indexPath.row==4) {
        cell.bgvw.image=[UIImage imageNamed:@"s5"];
    }if (indexPath.row==5) {
        cell.bgvw.image=[UIImage imageNamed:@"s6"];
    }
    return cell;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE_6 || IS_IPHONE_6S)
    {
        return CGSizeMake(186, 186);
    }
    else if (IS_IPHONE_6_PLUS || IS_IPHONE_6S_PLUS)
    {
        return CGSizeMake(206, 206);
    }
    else
    {
        return CGSizeMake(158 , 158);
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0) {
        AdmissionVC *controller = [[AdmissionVC alloc]initWithNibName:@"AdmissionVC" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (indexPath.row==1) {
        ManagementVC *controller = [[ManagementVC alloc]initWithNibName:@"ManagementVC" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
    }
    
    else if (indexPath.row==2) {
        AboutUsVC2 *controller = [[AboutUsVC2 alloc]initWithNibName:@"AboutUsVC2" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (indexPath.row==3) {
        VisionVC *controller = [[VisionVC alloc]initWithNibName:@"VisionVC" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (indexPath.row==4) {
        ContactUsVC3 *controller = [[ContactUsVC3 alloc]initWithNibName:@"ContactUsVC3" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (indexPath.row==5) {
        FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
        controller.isfrom=@"scareer";
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
