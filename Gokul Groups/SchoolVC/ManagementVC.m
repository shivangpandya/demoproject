//
//  ManagementVC.m
//  Gokul Groups
//
//  Created by Shivang on 28/09/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import "ManagementVC.h"

@interface ManagementVC (){
    
    IBOutlet UIView * vw1;
    IBOutlet UIView * vw2;

    IBOutlet UITextView*msgvw1;
    IBOutlet UITextView*msgvw2;

    IBOutlet UILabel *lbl1;
    IBOutlet UILabel *lbl2;

}

@end

@implementation ManagementVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNeedsStatusBarAppearanceUpdate];

    lbl1.layer.borderWidth=2.0f;
    lbl1.layer.borderColor=[UIColor redColor].CGColor;
    lbl1.layer.cornerRadius=3.0f;
    
    lbl2.layer.borderWidth=2.0f;
    lbl2.layer.borderColor=[UIColor redColor].CGColor;
    lbl2.layer.cornerRadius=3.0f;

    msgvw1.editable = NO;
    msgvw2.editable = NO;
    

}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(IBAction)btnbackclick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)btn1click:(id)sender{
    

    
    NSString * str = @"<b>'Until we are educating every kid in a fantastic way, there is no shortage of things to do.'</b> <br> <p align=\"right\"><b>-Bill Gates</b></p><br>It is my firm belief that “school system” must enable the individual child to get a get a quality education in an efficient way. Sunflower group's ongoing education journey has been accomplished with the goal of providing children a unique learning experience in an environment that enables them to gain knowledge while enhancing  their innate talents and will always continue moving towards our goal with relentless efforts. In June 2014 our school will have completed eight years of this journey and during this period Sunflower Group has carved a niche for itself in the field of “Quality Education” and “Academic Excellence”.<br><br>Utilizing our years of experience and expertise in education, we have also built our presence in the corporate educational consulting and training arena by offering Educational Management consultancy with fully developed ERP solution  for schools from this year. We have built the necessary foundation to establish ourselves as a key performer in education.<br><br>I am aware that our current and future success would not be possible without the continuous support and trust of our parents; they are the driving force behind Sunflower Education Group. I am certain that your ongoing support for Sunflower group will motivate us to overcome any challenges in future for expanding and growing as one of the leasing educational hubs in Gujarat.";
    
    str = [str stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-size:%fpx;}</style>",16.0]];
    
    msgvw1.attributedText = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUTF8StringEncoding]options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}documentAttributes:nil error:nil];
    


    
    vw1.frame=self.view.frame;
    vw1.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
    [self.view addSubview:vw1];
    
    [msgvw1 scrollRectToVisible:CGRectMake(0,0,1,1) animated:YES];

}

-(IBAction)close1click:(id)sender{
    [vw1 removeFromSuperview];
}

-(IBAction)close2click:(id)sender{
    [vw2 removeFromSuperview];
}

-(IBAction)btn2click:(id)sender{
    
    NSString * str = @"My personal feelings that in today's rapidly changing scenario, schools have the most responsible and important role to play and thus educational institutes must always crave for bringing quality and effectiveness in school education. Sunflower School is moving towards the same goal.<br><br>The single most important gift we can bestow upon our youth is 'Education' and its our greatest responsibility to do so. We consider it our bounden duty in Sunflower School to ensure that every pupil in our charge is educated to fullest extent of its capability and is prepared academically, morally and socially to take society with its best prospects of success and fulfillment<br><br>It is our task to bring together the influences necessary to make this happen in every case -  excellent facilities, good teaching and responsible parenthood, caring social leadership – to make sure every student is correctly accessed for potential and fully developed.<br><br>Its is the endeavor of Sunflower Group to make the academic life, a smooth journey full of discovery and enjoyment. I extend my warm wishes to the Principal, Staff and Students of Sunflower School for the best of their performance.<br><br>Sunflower School is an institution which by God's grace, Greater partnership and hard work of team and faith of parents shall bring Jamnagar to the map of prominent educational institutes of Gujarat.<br><br>My heart fills with pride and pleasure as I perceive the progress that will be made at Sunflower-Jamnagar. The seeds of idea of a unique school sown will soon take firm root and this school will grow into stronger sapling.<br><br>May Sapling grow into sturdy tree and spread its branches!";
    
    str = [str stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-size:%fpx;}</style>",16.0]];
    
    msgvw2.attributedText = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUTF8StringEncoding]options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}documentAttributes:nil error:nil];
    
    vw2.frame=self.view.frame;
    vw2.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
    [self.view addSubview:vw2];
    [msgvw2 scrollRectToVisible:CGRectMake(0,0,1,1) animated:YES];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
