//
//  AboutUsVC2.m
//  Gokul Groups
//
//  Created by Shivang on 28/09/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import "AboutUsVC2.h"

@interface AboutUsVC2 (){
    IBOutlet UITextView *tfmsg;
    
}


@end

@implementation AboutUsVC2

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNeedsStatusBarAppearanceUpdate];
    
    tfmsg.editable = NO;
    
    
    
}
-(void)viewDidAppear:(BOOL)animated{
    [tfmsg scrollRectToVisible:CGRectMake(0,0,1,1) animated:YES];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    NSString * str = @"<b>Sunflower Group </b>is a premier educational group which deals in providing quality and unique educational services through continuous innovation in field of education. Established in 1999, Rajkot based Sunflower Group is on path of becoming the leading Premier Educational Group in Gujarat.<br><br><b>Sunflower Group </b>currently operates 9 school affiliated with GSEB and CBSE having total student strength of above 3000. The Group has 2 own school and 7 schools are managed by the Group.<br><br><b>Sunflower Group </b>mainly deals in the following educational services:<br>-> Own Schools.<br>-> Physical Management of Other Schools.<br>-> e School Consultancy (ERP based school consultancy)<br> -> e Pre-School Franchise.<br><br><b>Sunflower Group </b>is developing fast into its new horizons with innovative and exclusive educational products. We have grandeur vision to become one of the leading educational groups in Gujarat.<br><br>We at <b>Sunflower Group, </b>sincerely believe in contributing our best in what ever we do so as to surpass our own benchmarks and create an enviable position for ourself and make our presence felt.";
    
    str = [str stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-size:%fpx;}</style>",16.0]];
    
    tfmsg.attributedText = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUTF8StringEncoding]options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}documentAttributes:nil error:nil];

}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(IBAction)btnbackclick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
