//
//  VisionVC.m
//  Gokul Groups
//
//  Created by Shivang on 28/09/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import "VisionVC.h"

@interface VisionVC (){
    IBOutlet UITextView *tfmsg;
    
}



@end

@implementation VisionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNeedsStatusBarAppearanceUpdate];
    
    tfmsg.editable = NO;

    
    
}
-(void)viewDidAppear:(BOOL)animated{
    [tfmsg scrollRectToVisible:CGRectMake(0,0,1,1) animated:YES];
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    NSString * str = @"To nurture progressive thinking individuals by employing latest instructional technology through 3 integrated schools:<br><br><b>->Sanskar (Academic Block)</b><br>To enhance knowledge which develops creative thinking and channelizes potential for an optimum performance<br><br><b>-> Pathshala (School for Exposure)</b><br>To provide exposure for laying the core foundation of a students’ personality and prepare them for the future<br><br><b>-> Eklavya (Excellence in Sports)</b><br>To equip students with the required skills of the game chosen and physical fitness<br><br>To imbibe Indian culture and heritage through a blend of activities to make students the global citizens of tomorrow.<br><br>To develop proficiency in the National and Global Language.";
    
    str = [str stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-size:%fpx;}</style>",16.0]];
    
    tfmsg.attributedText = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUTF8StringEncoding]options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding),NSFontAttributeName:[UIFont systemFontOfSize:16]}documentAttributes:nil error:nil];
    
    [tfmsg scrollRectToVisible:CGRectMake(0,0,1,1) animated:YES];

    
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(IBAction)btnbackclick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
