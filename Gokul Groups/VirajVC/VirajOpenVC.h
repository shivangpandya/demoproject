//
//  VirajOpenVC.h
//  Gokul Groups
//
//  Created by Shivang on 26/08/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OfferCell.h"
#import "ViewController.h"
#import "MBProgressHUD.h"

@interface VirajOpenVC : UIViewController{
    
    IBOutlet UICollectionView* OfferCollectionView;
    
    IBOutlet UIView * vwb;
    IBOutlet UIWebView * webview;
    
    MBProgressHUD *HUD;

}

-(IBAction)backbtnclick:(id)sender;
-(IBAction)backbtnclick1:(id)sender;

@end
