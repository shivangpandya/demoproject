//
//  VirajOpenVC.m
//  Gokul Groups
//
//  Created by Shivang on 26/08/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import "VirajOpenVC.h"
#import "FormVC.h"
#import "ContactUsVC2.h"
@interface VirajOpenVC ()<UIWebViewDelegate>

@end

@implementation VirajOpenVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNeedsStatusBarAppearanceUpdate];
    
    [OfferCollectionView registerNib:[UINib nibWithNibName:@"OfferCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    OfferCollectionView.backgroundColor = [UIColor clearColor];

    //desktop
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
  
}

-(IBAction)backbtnclick:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
   
}

-(IBAction)backbtnclick1:(id)sender{
    [vwb removeFromSuperview];
}

#pragma mark - UICollectionView Delegate Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 3;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    OfferCell *cell = (OfferCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    if (indexPath.row==0) {
        cell.bgvw.image=[UIImage imageNamed:@"v4"];
    }
    if (indexPath.row==1) {
        cell.bgvw.image=[UIImage imageNamed:@"c9"];
    }if (indexPath.row==2) {
        cell.bgvw.image=[UIImage imageNamed:@"v3"];
    }
    
    return cell;
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPHONE_6 || IS_IPHONE_6S)
    {
        return CGSizeMake(186, 186);
    }
    else if (IS_IPHONE_6_PLUS || IS_IPHONE_6S_PLUS)
    {
        return CGSizeMake(206, 206);
    }
    else
    {
        return CGSizeMake(158 , 158);
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
     if (indexPath.row==0) {
      
//         dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//             
//              NSLog(@"Downloading Started");
//             
//             NSData *pdfData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:@"http://gokulgroups.com/Services/images/brochure.pdf"]];
//             
//             // Store the Data locally as PDF File
//             NSString *resourceDocPath = [[NSString alloc] initWithString:[[[[NSBundle mainBundle] resourcePath] stringByDeletingLastPathComponent]stringByAppendingPathComponent:@"Documents"]];
//             
//             NSString *filePath = [resourceDocPath
//                                   stringByAppendingPathComponent:@"brochure.pdf"];
//             dispatch_async(dispatch_get_main_queue(), ^{
//                 
//                 NSLog(@"filepath : %@",filePath);
//                 [pdfData writeToFile:filePath atomically:YES];
//                 
//                 [self.view addSubview:vwb];
//                 vwb.frame = self.view.frame;
//                 NSURL *url = [NSURL fileURLWithPath:filePath];
//                 NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//                 
//                 [webview setUserInteractionEnabled:YES];
//                 [webview setDelegate:self];
//                 [webview loadRequest:requestObj];
//                 
//             });
//         });
        
         [self.view addSubview:vwb];
         vwb.frame = self.view.frame;

         HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
         HUD.mode = MBProgressHUDModeCustomView;
         HUD.customView = [ApplicationDelegate explosion];
         HUD.color = [UIColor clearColor];
         HUD.dimBackground = YES;
         

         [webview setUserInteractionEnabled:YES];
         [webview setDelegate:self];
         
         NSString *urlAddress = @"http://gokulgroups.com/Services/images/brochure.pdf";
         
         //Create a URL object.
         NSURL *url1 = [NSURL URLWithString:urlAddress];
         
         //URL Requst Object
         NSURLRequest *requestObj1 = [NSURLRequest requestWithURL:url1];
         [[NSURLCache sharedURLCache] removeCachedResponseForRequest:requestObj1];

         
         //Load the request in the UIWebView.
         [webview loadRequest:requestObj1];
        
    }
    
    else if (indexPath.row==1) {
        ContactUsVC2 *controller = [[ContactUsVC2 alloc]initWithNibName:@"ContactUsVC2" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (indexPath.row==2) {
        FormVC *controller = [[FormVC alloc]initWithNibName:@"FormVC" bundle:nil];
        controller.isfrom=@"Inquiry For Site";
        [self.navigationController pushViewController:controller animated:YES];    }
    
       
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    //SHOW HUD
   
    
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    //KILL HUD
    [HUD hide:YES];

}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    if(!webView.loading){
        //KILL HUD
        [HUD hide:YES];

    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
