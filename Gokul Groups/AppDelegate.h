//
//  AppDelegate.h
//  Gokul Groups
//
//  Created by Shivang on 07/06/17.
//  Copyright © 2017 Shivang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "Constants.h"
#import <SVProgressHUD.h>
#import "UserDefaultHelper.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    
    MBProgressHUD *HUD;
    UIImageView *explosion;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic)UIViewController*ViewController;
@property (nonatomic,retain) UINavigationController *nav;

- (UIImageView *) explosion;
+(AppDelegate *)sharedAppDelegate;


@end

